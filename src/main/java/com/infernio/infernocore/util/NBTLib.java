package com.infernio.infernocore.util;

public class NBTLib
{
    public static final int NBT_END = 0;
    public static final int NBT_BYTE = 1;
    public static final int NBT_SHORT = 2;
    public static final int NBT_INT = 3;
    public static final int NBT_LONG = 4;
    public static final int NBT_FLOAT = 5;
    public static final int NBT_DOUBLE = 6;
    public static final int NBT_BYTE_ARRAY = 7;
    public static final int NBT_STRING = 8;
    public static final int NBT_LIST = 9;
    public static final int NBT_COMPOUND = 10;
    public static final int NBT_INT_ARRAY = 11;
}
