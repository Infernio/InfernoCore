package com.infernio.infernocore.util;

public class EntityUtils
{
    public static String getEntityKey(String entityName)
    {
        return entityName.startsWith("entity.") ? entityName + ".name" : "entity." + entityName + ".name";
    }
}
