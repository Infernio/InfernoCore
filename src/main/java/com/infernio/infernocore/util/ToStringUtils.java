package com.infernio.infernocore.util;

import java.lang.reflect.Field;

import com.infernio.infernocore.InfernoCore;

public class ToStringUtils
{
    public static String reportDeclaredFields(Class<?> clazz, Object o)
    {
        String ret = "";
        Field[] fields = clazz.getDeclaredFields();
        for(int i = 0; i < fields.length; ++i)
        {
            try
            {
                ret += fields[i].getName() + " = " + fields[i].get(o);
            }
            catch(Throwable e)
            {
                InfernoCore.log.catching(e);
            }
            if(i + 1 < fields.length)
            {
                ret += ", ";
            }
        }
        return clazz.getSimpleName() + "[" + ret + "]";
    }

    public static String reportDeclaredFields(Object o)
    {
        return reportDeclaredFields(o.getClass(), o);
    }
}
