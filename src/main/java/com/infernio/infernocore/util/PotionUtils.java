package com.infernio.infernocore.util;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.init.MobEffects;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.math.MathHelper;

public class PotionUtils
{
    private static ArrayList<PotionEffect> negativePotions = new ArrayList<>();
    private static ArrayList<PotionEffect> positivePotions = new ArrayList<>();
    private static ArrayList<PotionEffect> neutralPotions = new ArrayList<>();
    private static ArrayList<PotionEffect> allPotions = new ArrayList<>();

    public static PotionEffect getRandomEffect(Random rand, int duration)
    {
        PotionEffect ret = allPotions.get(rand.nextInt(allPotions.size() - 1));
        return new PotionEffect(ret.getPotion(), duration, ret.getAmplifier(), ret.getIsAmbient(), ret.doesShowParticles());
    }

    public static PotionEffect getRandomNegativeEffect(Random rand, int duration)
    {
        PotionEffect ret = negativePotions.get(rand.nextInt(negativePotions.size() - 1));
        return new PotionEffect(ret.getPotion(), duration, ret.getAmplifier(), ret.getIsAmbient(), ret.doesShowParticles());
    }

    public static PotionEffect getRandomPositiveEffect(Random rand, int duration)
    {
        PotionEffect ret = positivePotions.get(rand.nextInt(positivePotions.size() - 1));
        return new PotionEffect(ret.getPotion(), duration, ret.getAmplifier(), ret.getIsAmbient(), ret.doesShowParticles());
    }

    public static PotionEffect getRandomNeutralEffect(Random rand, int duration)
    {
        PotionEffect ret = neutralPotions.get(rand.nextInt(neutralPotions.size() - 1));
        return new PotionEffect(ret.getPotion(), duration, ret.getAmplifier(), ret.getIsAmbient(), ret.doesShowParticles());
    }

    public static void improveEffect(PotionEffect effect)
    {
        if(effect == null)
        {
            return;
        }
        Potion potion = effect.getPotion();
        int duration = effect.getDuration();
        int finalDuration = 0;
        if(potion == MobEffects.BLINDNESS || potion == MobEffects.NAUSEA || potion == MobEffects.NIGHT_VISION || potion == MobEffects.HEALTH_BOOST || potion == MobEffects.ABSORPTION)
        {
            finalDuration = MathHelper.clamp(duration, 0, 200);
        }
        else if(potion == MobEffects.INSTANT_HEALTH || potion == MobEffects.INSTANT_DAMAGE)
        {
            finalDuration = 0;
        }
        else if(potion == MobEffects.POISON || potion == MobEffects.REGENERATION)
        {
            finalDuration = MathHelper.clamp(duration, 0, 350);
            return;
        }
        else
        {
            finalDuration = MathHelper.clamp(duration, 0, 500);
        }
        effect = new PotionEffect(potion, finalDuration, effect.getAmplifier(), effect.getIsAmbient(), effect.doesShowParticles());
    }

    static
    {
        // TODO Add all new potion effects
        positivePotions.add(new PotionEffect(MobEffects.SPEED, 0));
        positivePotions.add(new PotionEffect(MobEffects.HASTE, 0));
        positivePotions.add(new PotionEffect(MobEffects.STRENGTH, 0));
        positivePotions.add(new PotionEffect(MobEffects.INSTANT_HEALTH, 0));
        positivePotions.add(new PotionEffect(MobEffects.JUMP_BOOST, 0));
        positivePotions.add(new PotionEffect(MobEffects.REGENERATION, 0));
        positivePotions.add(new PotionEffect(MobEffects.RESISTANCE, 0));
        positivePotions.add(new PotionEffect(MobEffects.FIRE_RESISTANCE, 0));
        positivePotions.add(new PotionEffect(MobEffects.WATER_BREATHING, 0));
        positivePotions.add(new PotionEffect(MobEffects.INVISIBILITY, 0));
        positivePotions.add(new PotionEffect(MobEffects.NIGHT_VISION, 0));
        positivePotions.add(new PotionEffect(MobEffects.HEALTH_BOOST, 0));
        positivePotions.add(new PotionEffect(MobEffects.ABSORPTION, 0));
        positivePotions.add(new PotionEffect(MobEffects.LUCK, 0));
        negativePotions.add(new PotionEffect(MobEffects.SLOWNESS, 0));
        negativePotions.add(new PotionEffect(MobEffects.MINING_FATIGUE, 0));
        negativePotions.add(new PotionEffect(MobEffects.INSTANT_DAMAGE, 0));
        negativePotions.add(new PotionEffect(MobEffects.NAUSEA, 0));
        negativePotions.add(new PotionEffect(MobEffects.BLINDNESS, 0));
        negativePotions.add(new PotionEffect(MobEffects.HUNGER, 0));
        negativePotions.add(new PotionEffect(MobEffects.WEAKNESS, 0));
        negativePotions.add(new PotionEffect(MobEffects.POISON, 0));
        negativePotions.add(new PotionEffect(MobEffects.WITHER, 0));
        negativePotions.add(new PotionEffect(MobEffects.SATURATION, 0));
        negativePotions.add(new PotionEffect(MobEffects.UNLUCK, 0));
        neutralPotions.add(new PotionEffect(MobEffects.LEVITATION, 0));
        neutralPotions.add(new PotionEffect(MobEffects.GLOWING, 0));
        allPotions.addAll(positivePotions);
        allPotions.addAll(negativePotions);
        allPotions.addAll(neutralPotions);
    }
}
