package com.infernio.infernocore.util;

public class AchievementUtils
{
    public static String getAchievementName(String achievementName)
    {
        return "achievement." + achievementName;
    }

    public static String getAchievementDescription(String achievementName)
    {
        return "achievement." + achievementName + ".desc";
    }
}
