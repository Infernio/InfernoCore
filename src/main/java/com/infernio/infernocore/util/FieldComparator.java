package com.infernio.infernocore.util;

import java.lang.reflect.Field;
import java.util.Comparator;

public class FieldComparator implements Comparator<Field>
{
    @Override
    public int compare(Field f, Field f1)
    {
        if(f == null && f1 == null)
        {
            return 0;
        }
        if(f == null && f1 != null)
        {
            return -1;
        }
        if(f != null && f1 == null)
        {
            return 1;
        }
        return f.getName().compareTo(f1.getName());
    }
}
