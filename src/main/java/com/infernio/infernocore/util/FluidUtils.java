package com.infernio.infernocore.util;

public class FluidUtils
{
    public static String getFluidKey(String fluidName)
    {
        return fluidName.startsWith("fluid.") ? fluidName : "fluid." + fluidName;
    }
}
