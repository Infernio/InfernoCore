package com.infernio.infernocore.util;

import java.util.ArrayList;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemUtils
{
    public static String getItemKey(String itemName)
    {
        return "item." + itemName + ".name";
    }

    public static ItemStack addItemStack(ItemStack stack, IInventory inv)
    {
        if(stack != null && stack.getItem() != null)
        {
            if(!stack.getItem().getHasSubtypes())
            {
                // TODO Is this the proper replacement?
                if(!stack.getItem().getToolClasses(stack).isEmpty()) //stack.getItem().isItemTool(stack))
                {
                    int slot = findEmptySlot(inv);
                    if(slot > 0)
                    {
                        inv.setInventorySlotContents(slot, stack);
                        return null;
                    }
                }
                else
                {
                    int slot = findMatchingSlot(inv, stack.getItem(), 0);
                    if(slot >= 0)
                    {
                        ItemStack stack2 = inv.getStackInSlot(slot);
                        while(stack2.getCount() < stack2.getMaxStackSize() && stack.getCount() > 0)
                        {
                            stack.shrink(1);
                            stack2.grow(1);
                        }
                    }
                    else
                    {
                        slot = findEmptySlot(inv);
                        if(slot >= 0)
                        {
                            inv.setInventorySlotContents(slot, stack);
                            return null;
                        }
                    }
                }
                return stack;
            }
            int meta = stack.getMetadata();
            int slot = findMatchingSlot(inv, stack.getItem(), meta);
            if(slot >= 0)
            {
                ItemStack stack2 = inv.getStackInSlot(slot);
                while(stack2.getCount() < stack2.getMaxStackSize() && stack.getCount() > 0)
                {
                    stack.shrink(1);
                    stack2.grow(1);
                }
                return stack;
            }
            slot = findEmptySlot(inv);
            if(slot >= 0)
            {
                inv.setInventorySlotContents(slot, stack);
                return null;
            }
        }
        return stack;
    }

    public static int findEmptySlot(IInventory inv)
    {
        for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
        {
            if(inv.getStackInSlot(slot) == null)
            {
                return slot;
            }
        }
        return -1;
    }

    public static int findMatchingSlot(IInventory inv, Item item, int meta)
    {
        for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
        {
            ItemStack stack = inv.getStackInSlot(slot);
            if(stack != null && stack.getItem() != null && stack.getItem() == item && stack.getMetadata() == meta && stack.getCount() < inv.getInventoryStackLimit())
            {
                return slot;
            }
        }
        return -1;
    }

    // TODO Transition to using the official areItemStacksEqual methods
    public static boolean areItemStacksEqual(ItemStack stack1, ItemStack stack2, boolean compareStackSize)
    {
        if(stack1 == null && stack2 == null)
        {
            return true;
        }
        else if((stack1 == null) != (stack2 == null))
        {
            return false;
        }
        else if(stack1.getItem() == null && stack2.getItem() == null)
        {
            return true;
        }
        else if((stack1.getItem() == null) != (stack2.getItem() == null))
        {
            return false;
        }
        else if(stack1.getHasSubtypes() != stack2.getHasSubtypes())
        {
            return false;
        }
        else
        {
            if(stack1.getHasSubtypes())
            {
                if(stack1.getItem() == stack2.getItem() && stack1.getMetadata() == stack2.getMetadata())
                {
                    if(compareStackSize)
                    {
                        return stack1.getCount() == stack2.getCount();
                    }
                    return true;
                }
            }
            else
            {
                if(stack1.getItem() == stack2.getItem())
                {
                    if(compareStackSize)
                    {
                        return stack1.getCount() == stack2.getCount();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean areItemStacksEqualNBT(ItemStack stack1, ItemStack stack2, boolean compareStackSize)
    {
        if(stack1 == null && stack2 == null)
        {
            return true;
        }
        else if((stack1 == null) != (stack2 == null))
        {
            return false;
        }
        else if(stack1.getItem() == null && stack2.getItem() == null)
        {
            return true;
        }
        else if((stack1.getItem() == null) != (stack2.getItem() == null))
        {
            return false;
        }
        else if(stack1.getHasSubtypes() != stack2.getHasSubtypes())
        {
            return false;
        }
        else
        {
            if(stack1.getHasSubtypes())
            {
                if(stack1.getItem() == stack2.getItem() && stack1.getMetadata() == stack2.getMetadata())
                {
                    if(areNBTTagsEqual(stack1.getTagCompound(), stack2.getTagCompound()))
                    {
                        if(compareStackSize)
                        {
                            return stack1.getCount() == stack2.getCount();
                        }
                        return true;
                    }
                }
            }
            else
            {
                if(stack1.getItem() == stack2.getItem())
                {
                    if(areNBTTagsEqual(stack1.getTagCompound(), stack2.getTagCompound()))
                    {
                        if(compareStackSize)
                        {
                            return stack1.getCount() == stack2.getCount();
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean areNBTTagsEqual(NBTTagCompound tag1, NBTTagCompound tag2)
    {
        if(tag1 == null && tag2 == null)
        {
            return true;
        }
        else if((tag1 == null) != (tag2 == null))
        {
            return false;
        }
        else
        {
            return tag1.equals(tag2);
        }
    }

    public static ArrayList<ItemStack> findItems(IInventory inv, Item item, int meta)
    {
        ArrayList<ItemStack> ret = new ArrayList<>();
        ItemStack stack1 = new ItemStack(item, 1, meta);
        for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
        {
            ItemStack stack = inv.getStackInSlot(slot);
            if(areItemStacksEqual(stack, stack1, false))
            {
                ret.add(stack);
            }
        }
        return ret;
    }

    public static ArrayList<Integer> findItemSlots(IInventory inv, Item item, int meta)
    {
        ArrayList<Integer> ret = new ArrayList<>();
        ItemStack stack1 = new ItemStack(item, 1, meta);
        for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
        {
            ItemStack stack = inv.getStackInSlot(slot);
            if(areItemStacksEqual(stack, stack1, false))
            {
                ret.add(slot);
            }
        }
        return ret;
    }

    public static void dropItem(World world, BlockPos pos, ItemStack stack)
    {
        dropItem(world, pos.getX(), pos.getY(), pos.getZ(), stack);
    }

    public static void dropItem(World world, double x, double y, double z, ItemStack stack)
    {
        if(!world.isRemote)
        {
            EntityItem item = new EntityItem(world, x, y, z, stack);
            item.setDefaultPickupDelay();
            world.spawnEntity(item);
        }
    }

    public static void dropStacks(World world, BlockPos pos, Item item, int meta, int amount)
    {
        dropStacks(world, pos.getX(), pos.getY(), pos.getZ(), item, meta, amount, 64);
    }

    public static void dropStacks(World world, double x, double y, double z, Item item, int meta, int amount)
    {
        dropStacks(world, x, y, z, item, meta, amount, 64);
    }

    public static void dropStacks(World world, double x, double y, double z, Item item, int meta, int amount, int maxStackSize)
    {
        if(amount < maxStackSize)
        {
            dropItem(world, x, y, z, new ItemStack(item, amount, meta));
            return;
        }
        int numStacks = amount / maxStackSize;
        int left = amount % maxStackSize;
        for(int i = 0; i < numStacks; ++i)
        {
            dropItem(world, x, y, z, new ItemStack(item, maxStackSize, meta));
        }
        if(left > 0)
        {
            dropItem(world, x, y, z, new ItemStack(item, left, meta));
        }
    }
}
