package com.infernio.infernocore.util;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class PlayerUtils
{
    public static int getSlotForStack(InventoryPlayer inv, Item item, int meta)
    {
        for(int i = 0; i < inv.getSizeInventory(); ++i)
        {
            ItemStack stack = inv.getStackInSlot(i);
            if(stack != null && stack.getItem() == item && stack.getMetadata() == meta)
            {
                return i;
            }
        }
        return -1;
    }

    public static boolean consumeInventoryItemWithMeta(InventoryPlayer inv, Item item, int meta)
    {
        int slot = getSlotForStack(inv, item, meta);
        if(slot < 0 || slot > inv.getSizeInventory())
        {
            return false;
        }
        ItemStack stack = inv.getStackInSlot(slot);
        if(stack != null)
        {
            stack.shrink(1);
            if(stack.getCount() <= 0)
            {
                inv.setInventorySlotContents(slot, (ItemStack)null);
            }
            return true;
        }
        return false;
    }

    public static int getSlotForStackSize(InventoryPlayer inv, Item item, int meta, int amount)
    {
        for(int i = 0; i < inv.getSizeInventory(); ++i)
        {
            ItemStack stack = inv.getStackInSlot(i);
            if(stack != null && stack.getItem() == item && stack.getMetadata() == meta && stack.getCount() >= amount)
            {
                return i;
            }
        }
        return -1;
    }

    public static boolean consumeInventoryItemsWithMeta(InventoryPlayer inv, Item item, int meta, int amount)
    {
        int slot = getSlotForStack(inv, item, meta);
        if(slot < 0 || slot > inv.getSizeInventory())
        {
            return false;
        }
        ItemStack stack = inv.getStackInSlot(slot);
        if(stack != null)
        {
            stack.shrink(amount);
            if(stack.getCount() <= 0)
            {
                inv.setInventorySlotContents(slot, (ItemStack)null);
            }
            return true;
        }
        return false;
    }

    public static void giveItem(ItemStack item, World world, EntityPlayer player)
    {
        if(!player.inventory.addItemStackToInventory(item) && !world.isRemote)
        {
            world.spawnEntity(new EntityItem(world, player.posX, player.posY, player.posZ, item));
        }
    }

    public static RayTraceResult getFromPlayer(World world, EntityPlayer player, boolean useLiquids)
    {
        float f = player.rotationPitch;
        float f1 = player.rotationYaw;
        double d0 = player.posX;
        double d1 = player.posY + (double)player.getEyeHeight();
        double d2 = player.posZ;
        Vec3d vec3d = new Vec3d(d0, d1, d2);
        float f2 = MathHelper.cos(-f1 * 0.017453292F - (float)Math.PI);
        float f3 = MathHelper.sin(-f1 * 0.017453292F - (float)Math.PI);
        float f4 = -MathHelper.cos(-f * 0.017453292F);
        float f5 = MathHelper.sin(-f * 0.017453292F);
        float f6 = f3 * f4;
        float f7 = f2 * f4;
        double d3 = player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue();
        Vec3d vec3d1 = vec3d.addVector((double)f6 * d3, (double)f5 * d3, (double)f7 * d3);
        return world.rayTraceBlocks(vec3d, vec3d1, useLiquids, !useLiquids, false);
    }
}
