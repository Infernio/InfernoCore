package com.infernio.infernocore.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

public class TemporaryInventory implements ISidedInventory
{
    private int start;
    private int length;
    private ItemStack[] originalInventory;
    private int[] allSlots;

    public static TemporaryInventory wrap(ItemStack... inv)
    {
        return wrap(0, inv.length, inv);
    }

    public static TemporaryInventory wrap(int length, ItemStack... inv)
    {
        return wrap(0, length, inv);
    }

    public static TemporaryInventory wrap(int start, int length, ItemStack... inv)
    {
        TemporaryInventory ret = new TemporaryInventory(inv);
        ret.length = length;
        ret.start = start;
        ret.allSlots = new int[length];
        for(int i = start; i < length; ++i)
        {
            ret.allSlots[i] = i;
        }
        return ret;
    }

    public TemporaryInventory(ItemStack... inv)
    {
        this.originalInventory = inv;
        this.allSlots = new int[inv.length];
        for(int i = 0; i < inv.length; ++i)
        {
            this.allSlots[i] = i;
        }
    }

    @Override
    public int getSizeInventory()
    {
        return this.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return this.originalInventory[this.start + slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount)
    {
        slot += this.start;
        ItemStack stack = this.getStackInSlot(slot);
        if(stack != null)
        {
            if(stack.getCount() <= amount)
            {
                this.setInventorySlotContents(slot, null);
            }
            else
            {
                stack = stack.splitStack(amount);
                if(stack.getCount() <= 0)
                {
                    this.setInventorySlotContents(slot, null);
                }
            }
        }
        return stack;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        slot += this.start;
        this.originalInventory[slot] = stack;
        if(stack != null && stack.getCount() > this.getInventoryStackLimit())
        {
            stack.setCount(this.getInventoryStackLimit());
        }
    }

    @Override
    public ItemStack removeStackFromSlot(int slot)
    {
        slot += this.start;
        ItemStack ret = this.originalInventory[slot];
        this.originalInventory[slot] = null;
        return ret;
    }

    @Override
    public String getName()
    {
        return "TEMPORARY_INVENTORY";
    }

    @Override
    public boolean hasCustomName()
    {
        return false;
    }

    @Override
    public ITextComponent getDisplayName()
    {
        return new TextComponentString(this.getName());
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public void markDirty()
    {}

    @Override
    public boolean isUsableByPlayer(EntityPlayer player)
    {
        return true;
    }

    @Override
    public void openInventory(EntityPlayer player)
    {}

    @Override
    public void closeInventory(EntityPlayer player)
    {}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return true;
    }

    @Override
    public int getField(int id)
    {
        return 0;
    }

    @Override
    public void setField(int id, int value)
    {}

    @Override
    public int getFieldCount()
    {
        return 0;
    }

    @Override
    public void clear()
    {
        for(int i = this.start; i < this.length; ++i)
        {
            this.originalInventory[i] = null;
        }
    }

    @Override
    public int[] getSlotsForFace(EnumFacing side)
    {
        return null;
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
    {
        return true;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
    {
        return true;
    }

    @Override
    public boolean isEmpty()
    {
        for(int slot : this.allSlots)
        {
            ItemStack stack = this.getStackInSlot(slot);
            if(stack != null && !stack.isEmpty())
            {
                return false;
            }
        }
        return true;
    }
}
