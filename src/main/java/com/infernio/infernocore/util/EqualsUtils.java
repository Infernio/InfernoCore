package com.infernio.infernocore.util;

import java.lang.reflect.Field;
import java.util.Arrays;

import com.infernio.infernocore.InfernoCore;

public class EqualsUtils
{
    private static FieldComparator fieldComparator = new FieldComparator();

    public static boolean classEquals(Object o, Object o1)
    {
        return o.getClass() == o1.getClass();
    }

    public static boolean declaredFieldsEquals(Object o, Object o1)
    {
        try
        {
            if(classEquals(o, o1))
            {
                Class<?> c = o.getClass();
                Class<?> c1 = o1.getClass();
                Field[] fields = c.getDeclaredFields();
                Field[] fields1 = c1.getDeclaredFields();
                Arrays.sort(fields, fieldComparator);
                Arrays.sort(fields1, fieldComparator);
                for(int i = 0; i < fields.length; ++i)
                {
                    if(!fields[i].getName().equals(fields1[i].getName()))
                    {
                        return false;
                    }
                    else if(!equal(fields[i].get(o), fields1[i].get(o1)))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        catch(Throwable e)
        {
            InfernoCore.log.catching(e);
            return false;
        }
    }

    public static boolean equal(Object o, Object o1)
    {
        if(o == null && o1 == null)
        {
            return true;
        }
        if((o == null && o1 != null) || (o1 == null && o != null))
        {
            return false;
        }
        return o.equals(o1);
    }
}
