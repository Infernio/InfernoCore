package com.infernio.infernocore;

public class Resources
{
    public static final String MODID = "infernocore";
    public static final String NAME = "InfernoCore";
    public static final String CHANNEL = "InfernoCore";
    public static final int MIN_BLOCK_ID = 0;
    public static final int MAX_BLOCK_ID = 4095;
    public static final int MIN_ITEM_ID = 4096;
    public static final int MAX_ITEM_ID = 31999;
}
