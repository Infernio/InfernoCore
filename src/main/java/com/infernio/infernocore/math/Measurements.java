package com.infernio.infernocore.math;

public class Measurements
{
    public static final float KELVIN_SHIFT = 273.15F;
    public static final float FAHRENHEIT_SHIFT = 5.0F / 9.0F;
    public static final int SECONDS_PER_MINUTE = 60;
    public static final int MINUTES_PER_HOUR = 60;
    public static final int HOURS_PER_DAY = 24;
    public static final int DAYS_PER_YEAR = 365;

    public static float celsiusToKelvin(float celsius)
    {
        return celsius + KELVIN_SHIFT;
    }

    public static float kelvinToCelsius(float kelvin)
    {
        return kelvin - KELVIN_SHIFT;
    }

    public static float celsiusToFahrenheit(float celsius)
    {
        return celsius / FAHRENHEIT_SHIFT + 32;
    }

    public static float fahrenheitToCelsius(float fahrenheit)
    {
        return FAHRENHEIT_SHIFT * (fahrenheit - 32);
    }

    public static long secondsToMinutes(long seconds)
    {
        return seconds / SECONDS_PER_MINUTE;
    }

    public static long secondsToHours(long seconds)
    {
        return (seconds / SECONDS_PER_MINUTE) / MINUTES_PER_HOUR;
    }

    public static long secondsToDays(long seconds)
    {
        return ((seconds / SECONDS_PER_MINUTE) / MINUTES_PER_HOUR) / HOURS_PER_DAY;
    }

    public static long secondsToYears(long seconds)
    {
        return (((seconds / SECONDS_PER_MINUTE) / MINUTES_PER_HOUR) / HOURS_PER_DAY) / DAYS_PER_YEAR;
    }

    public static long minutesToSeconds(long minutes)
    {
        return minutes * SECONDS_PER_MINUTE;
    }

    public static long minutesToHours(long minutes)
    {
        return minutes / MINUTES_PER_HOUR;
    }

    public static long minutesToDays(long minutes)
    {
        return (minutes / MINUTES_PER_HOUR) / HOURS_PER_DAY;
    }

    public static long minutesToYears(long minutes)
    {
        return ((minutes / MINUTES_PER_HOUR) / HOURS_PER_DAY) / DAYS_PER_YEAR;
    }

    public static long hoursToSeconds(long hours)
    {
        return (hours * MINUTES_PER_HOUR) * SECONDS_PER_MINUTE;
    }

    public static long hoursToMinutes(long hours)
    {
        return hours * MINUTES_PER_HOUR;
    }

    public static long hoursToDays(long hours)
    {
        return hours / HOURS_PER_DAY;
    }

    public static long hoursToYears(long hours)
    {
        return (hours / HOURS_PER_DAY) / DAYS_PER_YEAR;
    }

    public static long daysToSeconds(long days)
    {
        return ((days * HOURS_PER_DAY) * MINUTES_PER_HOUR) * SECONDS_PER_MINUTE;
    }

    public static long daysToMinutes(long days)
    {
        return (days * HOURS_PER_DAY) * MINUTES_PER_HOUR;
    }

    public static long daysToHours(long days)
    {
        return days * HOURS_PER_DAY;
    }

    public static long daysToYears(long days)
    {
        return days / DAYS_PER_YEAR;
    }

    public static long yearsToSeconds(long years)
    {
        return (((years * DAYS_PER_YEAR) * HOURS_PER_DAY) * MINUTES_PER_HOUR) * SECONDS_PER_MINUTE;
    }

    public static long yearsToMinutes(long years)
    {
        return ((years * DAYS_PER_YEAR) * HOURS_PER_DAY) * MINUTES_PER_HOUR;
    }

    public static long yearsToHours(long years)
    {
        return (years * DAYS_PER_YEAR) * HOURS_PER_DAY;
    }

    public static long yearsToDays(long years)
    {
        return years * DAYS_PER_YEAR;
    }
}
