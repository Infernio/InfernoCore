package com.infernio.infernocore.math;

import java.util.Random;

import net.minecraft.util.math.BlockPos;

public class MathUtils
{
    public static Random RNG = new Random();

    public static int average(Integer... array)
    {
        int ret = 0;
        for(Integer i : array)
        {
            if(i != null)
            {
                ret += i.intValue();
            }
        }
        return ret / array.length;
    }

    public static float average(Float... array)
    {
        float ret = 0;
        for(Float f : array)
        {
            if(f != null)
            {
                ret += f.floatValue();
            }
        }
        return ret / array.length;
    }

    public static double average(Double... array)
    {
        double ret = 0;
        for(Double d : array)
        {
            if(d != null)
            {
                ret += d.doubleValue();
            }
        }
        return ret / array.length;
    }

    public static long average(Long... array)
    {
        long ret = 0;
        for(Long l : array)
        {
            if(l != null)
            {
                ret += l.longValue();
            }
        }
        return ret / array.length;
    }

    public static short average(Short... array)
    {
        int ret = 0;
        for(Short s : array)
        {
            if(s != null)
            {
                ret += s.shortValue();
            }
        }
        return (short)(ret / array.length);
    }

    public static byte average(Byte... array)
    {
        int ret = 0;
        for(Byte b : array)
        {
            if(b != null)
            {
                ret += b.byteValue();
            }
        }
        return (byte)(ret / array.length);
    }

    public static int sqrt_int(int i)
    {
        return (int)Math.sqrt(i);
    }

    public static float sqrt_float(float f)
    {
        return (float)Math.sqrt(f);
    }

    public static long sqrt_long(long l)
    {
        return (long)Math.sqrt(l);
    }

    public static short sqrt_sort(short s)
    {
        return (short)Math.sqrt(s);
    }

    public static byte sqrt_byte(byte b)
    {
        return (byte)Math.sqrt(b);
    }

    public static double percentValue(double base, double second)
    {
        return (second / base) * 100;
    }

    public static Vector2D middle(Vector2D point1, Vector2D point2)
    {
        return new Vector2D((point1.x + point2.x) / 2, (point1.y + point2.y));
    }

    public static int lineLength(Vector2D point1, Vector2D point2)
    {
        return sqrt_int((point2.x - point1.x) ^ 2 + (point2.y - point1.y) ^ 2);
    }

    public static int distance(BlockPos pos1, BlockPos pos2)
    {
        return distance(pos1.getX(), pos1.getY(), pos1.getZ(), pos2.getX(), pos2.getY(), pos2.getZ());
    }

    public static int distance(int x1, int y1, int z1, int x2, int y2, int z2)
    {
        int distanceX = x1 - x2;
        int distanceY = y1 - y2;
        int distanceZ = z1 - z2;
        return sqrt_int(distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ);
    }

    public static double distance_double(double x1, double y1, double z1, double x2, double y2, double z2)
    {
        double distanceX = x1 - x2;
        double distanceY = y1 - y2;
        double distanceZ = z1 - z2;
        return Math.sqrt(distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ);
    }

    public static int ensurePositiveOrZero(int i)
    {
        return i < 0 ? 0 : i;
    }

    public static int ensureNegativeOrZero(int i)
    {
        return i > 0 ? 0 : i;
    }

    public static int ensurePositive(int i)
    {
        return i <= 0 ? 1 : i;
    }

    public static int ensureNegative(int i)
    {
        return i >= 0 ? -1 : i;
    }
}
