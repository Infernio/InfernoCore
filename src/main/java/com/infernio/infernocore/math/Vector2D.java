package com.infernio.infernocore.math;

public class Vector2D implements Comparable<Vector2D>
{
    public int x;
    public int y;

    public Vector2D(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(Vector2D vec)
    {
        int ret = 0;
        if(vec.x < this.x)
        {
            ret = -1;
        }
        else if(vec.x > this.x)
        {
            ret = 1;
        }
        if(ret == 0)
        {
            if(vec.y < this.y)
            {
                ret = -1;
            }
            else if(vec.y > this.y)
            {
                ret = 1;
            }
        }
        return ret;
    }

    @Override
    public int hashCode()
    {
        int ret = 0;
        ret += 31 * this.x;
        ret += 31 * this.y;
        return ret;
    }

    @Override
    public boolean equals(Object other)
    {
        if(!(other instanceof Vector2D))
        {
            return false;
        }
        Vector2D vector = (Vector2D)other;
        return vector.x == this.x && vector.y == this.y;
    }

    @Override
    public String toString()
    {
        return "vec2D[" + this.x + "," + this.y + "]";
    }

    public static class DoubleVector2D implements Comparable<DoubleVector2D>
    {

        public double x;
        public double y;

        public DoubleVector2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(DoubleVector2D vec)
        {
            int ret = 0;
            if(vec.x < this.x)
            {
                ret = -1;
            }
            else if(vec.x > this.x)
            {
                ret = 1;
            }
            if(ret == 0)
            {
                if(vec.y < this.y)
                {
                    ret = -1;
                }
                else if(vec.y > this.y)
                {
                    ret = 1;
                }
            }
            return ret;
        }

        @Override
        public int hashCode()
        {
            int ret = 0;
            ret += 31 * this.x;
            ret += 31 * this.y;
            return ret;
        }

        @Override
        public boolean equals(Object other)
        {
            if(!(other instanceof Vector2D))
            {
                return false;
            }
            Vector2D vector = (Vector2D)other;
            return vector.x == this.x && vector.y == this.y;
        }

        @Override
        public String toString()
        {
            return "vec2D[" + this.x + "," + this.y + "]";
        }

    }
}