package com.infernio.infernocore.math;

public class ArrayUtils
{
    @SafeVarargs
    public static <T> int lengthExcludingNull(T... array)
    {
        if(array == null)
        {
            return 0;
        }
        int ret = 0;
        for(int i = 0; i < array.length; ++i)
        {
            if(array[i] != null)
            {
                ++ret;
            }
        }
        return ret;
    }

    @SafeVarargs
    public static <T> int findAmount(T object, T... array)
    {
        if(array == null)
        {
            return 0;
        }
        int ret = 0;
        for(int i = 0; i < array.length; ++i)
        {
            if(array[i] == null && object == null)
            {
                ++ret;
                continue;
            }
            if(array[i].equals(object))
            {
                ++ret;
            }
        }
        return ret;
    }

    @SafeVarargs
    public static <T> T findObjectSkipNull(int index, T... array)
    {
        if(array == null)
        {
            return null;
        }
        int e = 0;
        T ret = null;
        for(int i = 0; i < array.length; ++i)
        {
            if(e == index && array[i] != null)
            {
                ret = array[e];
            }
            else
            {
                e++;
            }
        }
        return ret;
    }

    public static Integer findBiggest(Integer... array)
    {
        if(array == null)
        {
            return null;
        }
        Integer biggest = null;
        for(Integer integer : array)
        {
            if(integer != null)
            {
                if(biggest == null || integer.intValue() > biggest.intValue())
                {
                    biggest = integer;
                }
            }
        }
        return biggest;
    }

    public static Integer findSmallest(Integer... array)
    {
        if(array == null)
        {
            return null;
        }
        Integer smallest = null;
        for(Integer integer : array)
        {
            if(integer != null)
            {
                if(smallest == null || integer.intValue() < smallest.intValue())
                {
                    smallest = integer;
                }
            }
        }
        return smallest;
    }

    public static boolean contains(boolean[] array, boolean value)
    {
        if(array == null)
        {
            return false;
        }
        for(boolean b : array)
        {
            if(b == value)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(int[] array, int value)
    {
        if(array == null)
        {
            return false;
        }
        for(int i : array)
        {
            if(i == value)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(byte[] array, byte value)
    {
        if(array == null)
        {
            return false;
        }
        for(byte b : array)
        {
            if(b == value)
            {
                return true;
            }
        }
        return false;
    }
}
