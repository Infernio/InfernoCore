package com.infernio.infernocore.container;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;

public class SlotTool extends Slot {

	public SlotTool(IInventory inventory, int id, int x, int y)
	{
		super(inventory, id, x, y);
	}

	@Override
	public int getSlotStackLimit()
	{
		return 1;
	}

	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return !this.getHasStack() && stack != null && stack.getItem() != null && stack.getItem() instanceof ItemTool;
	}

}
