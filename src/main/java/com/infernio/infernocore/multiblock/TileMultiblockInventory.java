package com.infernio.infernocore.multiblock;

import com.infernio.infernocore.util.NBTLib;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.FMLCommonHandler;

public abstract class TileMultiblockInventory extends TileMultiblock implements ISidedInventory
{
    protected ItemStack[] inv;
    protected final int[] ALL_SLOTS;
    protected static final int[] NO_SLOTS = new int[]{};

    public TileMultiblockInventory(int inventorySize)
    {
        this.inv = new ItemStack[inventorySize];
        this.ALL_SLOTS = new int[inventorySize];
        for(int i = 0; i < this.ALL_SLOTS.length; ++i)
        {
            this.ALL_SLOTS[i] = i;
        }
    }

    @Override
    public int getSizeInventory()
    {
        if(this.world.isRemote)
        {
            if(this.clientMaster)
            {
                return this.inv.length;
            }
            return 0;
        }
        if(this.isMaster)
        {
            if(!this.multiblock.isComplete())
            {
                return 0;
            }
            return this.inv.length;
        }
        return this.master == null ? 0 : ((TileMultiblockInventory)this.master).getSizeInventory();
    }

    @Override
    public ItemStack getStackInSlot(int slot)
    {
        if(this.world.isRemote)
        {
            if(this.clientMaster)
            {
                return this.inv[slot];
            }
            return null;
        }
        if(this.isMaster)
        {
            if(!this.multiblock.isComplete())
            {
                return null;
            }
            return this.inv[slot];
        }
        return this.master == null ? null : ((TileMultiblockInventory)this.master).getStackInSlot(slot);
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount)
    {
        if(this.world.isRemote)
        {
            if(this.clientMaster)
            {
                ItemStack stack = getStackInSlot(slot);
                if(stack != null)
                {
                    if(stack.getCount() <= amount)
                    {
                        this.setInventorySlotContents(slot, null);
                    }
                    else
                    {
                        stack = stack.splitStack(amount);
                        if(stack.getCount() <= 0)
                        {
                            this.setInventorySlotContents(slot, null);
                        }
                    }
                }
                return stack;
            }
            return null;
        }
        if(this.isMaster)
        {
            if(!this.multiblock.isComplete())
            {
                return null;
            }
            ItemStack stack = getStackInSlot(slot);
            if(stack != null)
            {
                if(stack.getCount() <= amount)
                {
                    this.setInventorySlotContents(slot, null);
                }
                else
                {
                    stack = stack.splitStack(amount);
                    if(stack.getCount() <= 0)
                    {
                        this.setInventorySlotContents(slot, null);
                    }
                }
            }
            return stack;
        }
        return this.master == null ? null : ((TileMultiblockInventory)this.master).decrStackSize(slot, amount);
    }

    @Override
    public ItemStack removeStackFromSlot(int slot)
    {
        ItemStack ret = this.inv[slot];
        this.inv[slot] = null;
        return ret;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        if(this.world.isRemote)
        {
            if(this.clientMaster)
            {
                this.inv[slot] = stack;
            }
            return;
        }
        if(this.isMaster)
        {
            this.inv[slot] = stack;
            return;
        }
        if(this.master != null)
        {
            ((TileMultiblockInventory)this.master).setInventorySlotContents(slot, stack);
        }
    }

    @Override
    public String getName()
    {
        return "container." + this.getID();
    }

    @Override
    public boolean hasCustomName()
    {
        return false;
    }

    @Override
    public ITextComponent getDisplayName()
    {
        return new TextComponentString(I18n.translateToLocal(this.getName()));
    }

    @Override
    public int getInventoryStackLimit()
    {
        if(this.world.isRemote)
        {
            if(this.clientMaster)
            {
                return 64;
            }
            return 0;
        }
        if(this.isMaster)
        {
            if(!this.multiblock.isComplete())
            {
                return 0;
            }
            return 64;
        }
        return this.master == null ? 0 : ((TileMultiblockInventory)this.master).getInventoryStackLimit();
    }

// TODO Find replacement
//    @Override
//    public boolean isUseableByPlayer(EntityPlayer player)
//    {
//        if(this.world.isRemote)
//        {
//            if(this.clientMaster)
//            {
//                return this.world.getTileEntity(this.pos) == this && player.getDistanceSq(this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5) < 64;
//            }
//            return false;
//        }
//        if(this.isMaster)
//        {
//            if(!this.multiblock.isComplete())
//            {
//                return false;
//            }
//            return this.world.getTileEntity(this.pos) == this && player.getDistanceSq(this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5) < 64;
//        }
//        return this.master == null ? false : ((TileMultiblockInventory)this.master).isUseableByPlayer(player);
//    }

    @Override
    public void openInventory(EntityPlayer player) {}

    @Override
    public void closeInventory(EntityPlayer player) {}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        if(this.world.isRemote)
        {
            if(this.clientMaster)
            {
                return this.isValidForSlot(slot, stack);
            }
            return false;
        }
        if(this.isMaster)
        {
            if(!this.multiblock.isComplete())
            {
                return false;
            }
            return this.isValidForSlot(slot, stack);
        }
        return this.master == null ? false : ((TileMultiblockInventory)this.master).isItemValidForSlot(slot, stack);
    }

    public abstract boolean isValidForSlot(int slot, ItemStack stack);

    @Override
    public int getField(int id)
    {
        return 0;
    }

    @Override
    public void setField(int id, int value) {}

    @Override
    public int getFieldCount()
    {
        return 0;
    }

    @Override
    public void clear()
    {
        for(int i = 0; i < this.inv.length; ++i)
        {
            this.inv[i] = null;
        }
    }

    @Override
    public int[] getSlotsForFace(EnumFacing side)
    {
        if(this.isMaster)
        {
            if(this.multiblock == null || !this.multiblock.isComplete())
            {
                return NO_SLOTS;
            }
            return this.ALL_SLOTS;
        }
        return this.master == null ? NO_SLOTS : ((TileMultiblockInventory)this.master).getSlotsForFace(side);
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack stack, EnumFacing side)
    {
        return true;
    }

    @Override
    public boolean canExtractItem(int slot, ItemStack stack, EnumFacing side)
    {
        return true;
    }

    @Override
    protected void writeMBToNBT(NBTTagCompound tag)
    {
        if(FMLCommonHandler.instance().getEffectiveSide().isClient())
        {
            return;
        }
        NBTTagList list = new NBTTagList();
        for(int i = 0; i < this.inv.length; i++)
        {
            ItemStack stack = this.inv[i];
            if(stack != null)
            {
                NBTTagCompound slot = new NBTTagCompound();
                slot.setByte("Slot", (byte)i);
                stack.writeToNBT(slot);
                list.appendTag(slot);
            }
        }
        tag.setTag("Inventory", list);
    }

    @Override
    protected void readMBFromNBT(NBTTagCompound tag)
    {
        if(FMLCommonHandler.instance().getEffectiveSide().isClient())
        {
            return;
        }
        NBTTagList list = tag.getTagList("Inventory", NBTLib.NBT_COMPOUND);
        for(int i = 0; i < list.tagCount(); i++)
        {
            NBTTagCompound slot = list.getCompoundTagAt(i);
            byte b = slot.getByte("Slot");
            if(b >= 0 && b < this.inv.length)
            {
                this.inv[b] = new ItemStack(slot);
            }
        }
    }
}
