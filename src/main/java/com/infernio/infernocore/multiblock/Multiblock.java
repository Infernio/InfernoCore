package com.infernio.infernocore.multiblock;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import com.infernio.infernocore.network.PacketHelper;
import com.infernio.infernocore.network.message.MessageNeighborRenderUpdate;
import com.infernio.infernocore.network.message.MessageUpdateMultiblockClient;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public final class Multiblock
{
    public final MultiblockSize size;
    public boolean modified;
    public boolean needsRefresh;
    public boolean needsNewMaster;
    public boolean needsNotifying;
    private int scanCounter;
    private int fullScanCounter;
    private World world;
    private TileMultiblock master;
    public final String type;
    public EnumMultiblockState state;
    private TreeMap<BlockPos, TileMultiblock> parts;

    public Multiblock(String type, int xSize, int ySize, int zSize, World world)
    {
        this(type, new MultiblockSize(xSize, ySize, zSize), world);
    }

    public Multiblock(String type, MultiblockSize size, World world)
    {
        this.type = type;
        this.size = size;
        this.state = EnumMultiblockState.UNKNOWN;
        this.world = world;
        this.parts = new TreeMap<>();
    }

    public boolean addPart(int x, int y, int z, TileMultiblock part)
    {
        return this.addPart(new BlockPos(x, y, z), part);
    }

    public boolean addPart(BlockPos pos, TileMultiblock part)
    {
        if(pos != null && part != null && this.parts.size() < this.size.totalSize)
        {
            Entry<BlockPos, TileMultiblock> entry;
            Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
            while(ite.hasNext())
            {
                entry = ite.next();
                BlockPos pos2 = entry.getKey();
                int xDistance = pos.getX() - pos2.getX();
                if(xDistance >= this.size.sizeX || xDistance <= -this.size.sizeX)
                {
                    return false;
                }
                int yDistance = pos.getY() - pos2.getY();
                if(yDistance >= this.size.sizeY || yDistance <= -this.size.sizeY)
                {
                    return false;
                }
                int zDistance = pos.getZ() - pos2.getZ();
                if(zDistance >= this.size.sizeZ || zDistance <= -this.size.sizeZ)
                {
                    return false;
                }
            }
            part.multiblock = this;
            part.master = this.master;
            part.isMaster = part.equals(this.master);
            this.parts.put(pos, part);
            this.needsNotifying = true;
            return true;
        }
        return false;
    }

    public TileMultiblock removePart(int x, int y, int z)
    {
        return this.removePart(new BlockPos(x, y, z));
    }

    public TileMultiblock removePart(BlockPos pos)
    {
        this.state = EnumMultiblockState.UNKNOWN;
        this.modified = true;
        TileMultiblock part = this.parts.get(pos);
        if(part == null)
        {
            return null;
        }
        if(part.equals(this.master))
        {
            this.needsNewMaster = true;
            this.needsNotifying = true;
            part.multiblock = null;
            part.master = null;
            part.isMaster = false;
            return this.parts.remove(pos);
        }
        this.needsNotifying = true;
        part.multiblock = null;
        part.master = null;
        part.isMaster = false;
        return this.parts.remove(pos);
    }

    public boolean hasPart(int x, int y, int z)
    {
        return this.hasPart(new BlockPos(x, y, z));
    }

    public boolean hasPart(BlockPos pos)
    {
        return this.parts.containsKey(pos);
    }

    public void tick()
    {
        if(this.master == null || this.needsNewMaster)
        {
            this.assignMaster();
            this.needsNewMaster = false;
        }
        if(this.needsRefresh)
        {
            this.refresh();
            this.needsRefresh = false;
        }
        if(this.needsNotifying)
        {
            this.notifyAllNeighbors();
            this.needsNotifying = false;
        }
        if(this.modified)
        {
            this.fullScan();
            this.modified = false;
            return;
        }
        if(this.scanCounter < 40)
        {
            ++this.scanCounter;
        }
        else
        {
            this.scanCounter = 0;
            this.rescan();
        }
        if(this.fullScanCounter < 80)
        {
            ++this.fullScanCounter;
        }
        else
        {
            this.fullScanCounter = 0;
            this.fullScan();
        }
    }

    public void fullScan()
    {
        this.needsRefresh = true;
        this.needsNotifying = true;
        Entry<BlockPos, TileMultiblock> entry;
        Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
        while(ite.hasNext())
        {
            entry = ite.next();
            TileMultiblock part = entry.getValue();
            BlockPos pos = entry.getKey();
            Entry<BlockPos, TileMultiblock> entry2;
            Iterator<Entry<BlockPos, TileMultiblock>> ite2 = this.parts.entrySet().iterator();
            while(ite2.hasNext())
            {
                entry2 = ite2.next();
                BlockPos pos2 = entry2.getKey();
                if(pos2.equals(pos))
                {
                    continue;
                }
                boolean removed = false;
                int xDistance = pos.getX() - pos2.getX();
                if(xDistance >= this.size.sizeX || xDistance <= -this.size.sizeX)
                {
                    removed = true;
                    if(part.isMaster)
                    {
                        this.needsNewMaster = true;
                    }
                    ite.remove();
                }
                int yDistance = pos.getY() - pos2.getY();
                if(yDistance >= this.size.sizeY || yDistance <= -this.size.sizeY)
                {
                    if(!removed)
                    {
                        if(part.isMaster)
                        {
                            this.needsNewMaster = true;
                        }
                        ite.remove();
                        removed = true;
                    }
                }
                int zDistance = pos.getZ() - pos2.getZ();
                if(zDistance >= this.size.sizeZ || zDistance <= -this.size.sizeZ)
                {
                    if(!removed)
                    {
                        if(part.isMaster)
                        {
                            this.needsNewMaster = true;
                        }
                        ite.remove();
                    }
                }
            }
        }
    }

    public void rescan()
    {
        this.needsNotifying = true;
        boolean hasMaster = false;
        Entry<BlockPos, TileMultiblock> entry;
        Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
        boolean assignMaster = false;
        while(ite.hasNext())
        {
            entry = ite.next();
            BlockPos pos = entry.getKey();
            TileMultiblock part = entry.getValue();
            IBlockState state = this.world.getBlockState(pos);
            boolean removed = false;
            if(!state.getBlock().isAir(state, this.world, pos))
            {
                if(part.isMaster)
                {
                    assignMaster = true;
                }
                part.multiblock = null;
                part.master = null;
                part.isMaster = false;
                ite.remove();
                removed = true;
                this.state = EnumMultiblockState.INCOMPLETE;
                this.modified = true;
            }
            if(!part.getID().equals(this.type))
            {
                if(!removed)
                {
                    if(part.isMaster)
                    {
                        assignMaster = true;
                    }
                    part.multiblock = null;
                    part.master = null;
                    part.isMaster = false;
                    ite.remove();
                    removed = true;
                    this.state = EnumMultiblockState.INCOMPLETE;
                    this.modified = true;
                }
            }
            if(part.isMaster)
            {
                if(removed)
                {
                    assignMaster = true;
                }
                if(!hasMaster)
                {
                    this.master = part;
                    hasMaster = true;
                }
                else
                {
                    part.isMaster = false;
                    part.master = this.master;
                }
            }
        }
        if(this.parts.size() == this.size.totalSize)
        {
            this.state = EnumMultiblockState.COMPLETE;
        }
        else
        {
            this.state = EnumMultiblockState.INCOMPLETE;
        }
        if(assignMaster)
        {
            this.needsNewMaster = true;
        }
        if(!hasMaster)
        {
            this.needsNewMaster = true;
        }
        else
        {
            this.needsRefresh = true;
        }
    }

    public void refresh()
    {
        Entry<BlockPos, TileMultiblock> entry;
        Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
        while(ite.hasNext())
        {
            entry = ite.next();
            TileMultiblock part = entry.getValue();
            BlockPos pos = entry.getKey();
            part.multiblock = this;
            part.master = this.master;
            part.isMaster = part.equals(this.master);
            boolean completed = this.isComplete();
            PacketHelper.sendToDimension(new MessageUpdateMultiblockClient(pos.getX(), pos.getY(), pos.getZ(), completed), this.world.provider.getDimension());
        }
    }

    public void assignMaster()
    {
        Entry<BlockPos, TileMultiblock> entry;
        Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
        while(ite.hasNext())
        {
            entry = ite.next();
            TileMultiblock part = entry.getValue();
            if(!part.isMaster)
            {
                part.isMaster = true;
                part.master = part;
                this.master = part;
                break;
            }
        }
        this.needsNotifying = true;
        this.needsRefresh = true;
    }

    public void combineMultiblocks(Multiblock other)
    {
        if(other == null || this.parts == null || other.parts == null)
        {
            return;
        }
        int size = this.parts.size();
        int size2 = other.parts.size();
        if(size > size2)
        {
            this.doCombineMultiblocks(other);
            return;
        }
        else if(size < size2)
        {
            other.doCombineMultiblocks(this);
            return;
        }
        else if(size == size2)
        {
            boolean choice = this.world.rand.nextBoolean();
            if(choice)
            {
                other.doCombineMultiblocks(this);
            }
            else
            {
                this.doCombineMultiblocks(other);
            }
        }
    }

    private void doCombineMultiblocks(Multiblock other)
    {
        Set<Entry<BlockPos, TileMultiblock>> entrySet = other.parts.entrySet();
        Entry<BlockPos, TileMultiblock>[] entries = entrySet.toArray(new Entry[entrySet.size()]);
        for(int i = 0; i < entrySet.size(); ++i)
        {
            Entry<BlockPos, TileMultiblock> entry = entries[i];
            BlockPos pos = entry.getKey();
            Entry<BlockPos, TileMultiblock> entry2;
            Iterator<Entry<BlockPos, TileMultiblock>> ite2 = this.parts.entrySet().iterator();
            while(ite2.hasNext())
            {
                entry2 = ite2.next();
                BlockPos pos2 = entry2.getKey();
                int xDistance = pos.getX() - pos2.getX();
                if(xDistance == 1 || xDistance == -1)
                {
                    other.parts.remove(pos);
                    this.addPart(pos, entry.getValue());
                    break;
                }
                int yDistance = pos.getY() - pos2.getY();
                if(yDistance == 1 || yDistance == -1)
                {
                    other.parts.remove(pos);
                    this.addPart(pos, entry.getValue());
                    break;
                }
                int zDistance = pos.getZ() - pos2.getZ();
                if(zDistance == 1 || zDistance == -1)
                {
                    other.parts.remove(pos);
                    this.addPart(pos, entry.getValue());
                    break;
                }
            }
        }
    }

    public World getWorld()
    {
        return this.world;
    }

    public void setWorld(World world)
    {
        this.world = world;
    }

    public void setMaster(TileMultiblock master)
    {
        if(!master.getID().equals(this.type))
        {
            return;
        }
        if(this.master == null)
        {
            this.master = master;
            TileEntity tile = master;
            if(!this.hasPart(tile.getPos()))
            {
                this.addPart(tile.getPos(), master);
            }
        }
    }

    public boolean isComplete()
    {
        return this.state.equals(EnumMultiblockState.COMPLETE);
    }

    public void notifyAllNeighbors()
    {
        Entry<BlockPos, TileMultiblock> entry;
        Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
        while(ite.hasNext())
        {
            entry = ite.next();
            if(entry != null)
            {
                BlockPos pos = entry.getKey();
                if(pos != null)
                {
                    this.world.notifyNeighborsOfStateChange(pos, this.world.getBlockState(pos).getBlock(), false);
                    PacketHelper.sendToDimension(new MessageNeighborRenderUpdate(pos.getX(), pos.getY(), pos.getZ()), this.world.provider.getDimension());
                }
            }
        }
    }

    public void checkStatus()
    {
        Entry<BlockPos, TileMultiblock> entry;
        Iterator<Entry<BlockPos, TileMultiblock>> ite = this.parts.entrySet().iterator();
        while(ite.hasNext())
        {
            entry = ite.next();
            if(entry == null || entry.getKey() == null || entry.getValue() == null)
            {
                this.state = EnumMultiblockState.INCOMPLETE;
                return;
            }
        }
        if(this.parts.size() == this.size.totalSize)
        {
            this.state = EnumMultiblockState.COMPLETE;
        }
        else
        {
            this.state = EnumMultiblockState.INCOMPLETE;
        }
    }

    public static enum EnumMultiblockState
    {
        COMPLETE, INCOMPLETE, UNKNOWN;
    }
}
