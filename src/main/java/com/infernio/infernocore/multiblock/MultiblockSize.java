package com.infernio.infernocore.multiblock;

public class MultiblockSize
{
    public final int sizeX;
    public final int sizeY;
    public final int sizeZ;
    public final int totalSize;

    public MultiblockSize(int xSize, int ySize, int zSize)
    {
        this.sizeX = xSize;
        this.sizeY = ySize;
        this.sizeZ = zSize;
        this.totalSize = xSize * ySize * zSize;
    }
}
