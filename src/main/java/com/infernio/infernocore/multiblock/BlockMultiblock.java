package com.infernio.infernocore.multiblock;

import com.infernio.infernocore.block.BlockTile;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving.SpawnPlacementType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;

public abstract class BlockMultiblock extends BlockTile
{
    protected Block block;

    public BlockMultiblock(Block block, Material material)
    {
        super(material);
        this.block = block;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if(world.isRemote)
        {
            return true;
        }
        try
        {
            TileEntity tile = world.getTileEntity(pos);
            if(tile == null || !(tile instanceof TileMultiblock))
            {
                return false;
            }
            if(player.isSneaking() && player.getHeldItemMainhand() != null)
            {
                return false;
            }
            TileMultiblock part = (TileMultiblock)tile;
            if(part.multiblock == null || part.master == null)
            {
                return false;
            }
            if(part.isMaster)
            {
                if(!part.multiblock.isComplete())
                {
                    return false;
                }
                player.swingArm(hand);
                this.openGUI(player, world, pos);
                return true;
            }
            TileMultiblock master = part.master;
            this.block.onBlockActivated(world, master.getPos(), world.getBlockState(master.getPos()), player, hand, side, hitX, hitY, hitZ);
            return true;
        }
        catch(Throwable e)
        {
            return false;
        }
    }

    @Override
    public boolean canCreatureSpawn(IBlockState state, IBlockAccess world, BlockPos pos, SpawnPlacementType type)
    {
        if(FMLCommonHandler.instance().getEffectiveSide().isClient())
        {
            return false;
        }
        if(type != SpawnPlacementType.IN_AIR)
        {
            TileEntity tile = world.getTileEntity(pos);
            if(tile instanceof TileMultiblock)
            {
                return ((TileMultiblock)tile).canCreatureSpawn(type);
            }
        }
        return true;
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase entity, ItemStack stack)
    {
        if(!world.isRemote)
        {
            TileEntity tile = world.getTileEntity(pos);
            if(tile instanceof TileMultiblock)
            {
                TileMultiblock part = (TileMultiblock)tile;
                TileEntity tileTop = world.getTileEntity(pos.add(0, 1, 0));
                TileEntity tileBottom = world.getTileEntity(pos.add(0, -1, 0));
                TileEntity tileSide1 = world.getTileEntity(pos.add(1, 0, 0));
                TileEntity tileSide2 = world.getTileEntity(pos.add(-1, 0, 0));
                TileEntity tileSide3 = world.getTileEntity(pos.add(0, 0, 1));
                TileEntity tileSide4 = world.getTileEntity(pos.add(0, 0, -1));
                if(this.canPlace(tileTop) && this.canPlace(tileBottom) && this.canPlace(tileSide1) && this.canPlace(tileSide2) && this.canPlace(tileSide3) && this.canPlace(tileSide4))
                {
                    part.scanArea();
                    if(part.multiblock == null)
                    {
                        world.notifyNeighborsOfStateChange(pos, state.getBlock(), false);
                    }
                    else
                    {
                        part.multiblock.needsNotifying = true;
                    }
                    return;
                }
                state.getBlock().dropBlockAsItem(world, pos, state, 0);
                world.setBlockToAir(pos);
            }
        }
    }

    protected boolean canPlace(TileEntity tile)
    {
        if(tile != null && tile instanceof TileMultiblock)
        {
            TileMultiblock part = (TileMultiblock)tile;
            if(part.multiblock != null)
            {
                part.multiblock.rescan();
                return !part.multiblock.isComplete();
            }
            return true;
        }
        return true;
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state)
    {
        TileMultiblock tile = (TileMultiblock)world.getTileEntity(pos);
        if(tile != null && tile.multiblock != null)
        {
            tile.multiblock.removePart(tile.getPos());
        }
        super.breakBlock(world, pos, state);
    }
}
