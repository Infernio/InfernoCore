package com.infernio.infernocore;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.infernio.infernocore.network.PacketHelper;
import com.infernio.infernocore.network.Packets;
import com.infernio.infernocore.proxy.Proxy;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Resources.MODID, useMetadata=true)
public class InfernoCore
{
    public static final Logger log = LogManager.getLogger(Resources.NAME);
    public static boolean showRawNames = false;

    @SidedProxy(serverSide = "com.infernio.infernocore.proxy.Proxy", clientSide = "com.infernio.infernocore.proxy.ClientProxy")
    public static Proxy proxy;

    @EventHandler
    public void preLoad(FMLPreInitializationEvent event)
    {
        loadConfig(event);

        proxy.registerListener();
    }

    private static void loadConfig(FMLPreInitializationEvent event)
    {
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());

        config.load();

        showRawNames = config.get("General", "infernoCore.general.showRawNames", false, "Setting this to true will show which mod an item comes from when hovering over it. Will also show raw names of items and blocks while holding shift in gold. These are the names that minecraft now uses instead of numbers.").getBoolean(false);
        Packets.PACKET_SPAWN_PARTICLE_RANGE = config.get("Graphics", "infernoCore.graphics.particleRange", 32, "The range in which particles spawned by InfernoCore mods will be visible.").getInt(32);
        Packets.PACKET_PLAY_EVENT_RANGE = config.get("Graphics", "infernoCore.graphics.auxSFXRange", 32, "The range in which Aux SFX effects played by InfernoCore mods will be visible / audible.").getInt(16);

        config.save();
    }

    @EventHandler
    public void load(FMLInitializationEvent event)
    {
        PacketHelper.init();
    }

    @EventHandler
    public void postLoad(FMLPostInitializationEvent event)
    {
        proxy.registerKeyHandler();
    }
}
