package com.infernio.infernocore.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import com.infernio.infernocore.InfernoCore;

public class IOUtils
{
    public static String getCurrentLocation()
    {
        return new File("").getAbsolutePath();
    }

    public static boolean getBoolean(String dataLocation, String fileName, String bool)
    {
        try
        {
            File dir = new File(getCurrentLocation() + File.separator + "mods" + File.separator + dataLocation);
            dir.mkdirs();
            File file = new File(dir.getAbsolutePath() + File.separator + fileName);
            if(!file.exists())
            {
                file.createNewFile();
                return false;
            }
            try(Scanner s = new Scanner(file))
            {
                String line;
                if(!s.hasNext())
                {
                    return false;
                }
                while(s.hasNextLine())
                {
                    line = s.nextLine().trim();
                    if(line.startsWith(bool))
                    {
                        try
                        {
                            Boolean b = Boolean.parseBoolean(line.substring(line.indexOf("=")));
                            return b.booleanValue();
                        }
                        catch(Throwable e)
                        {
                            return false;
                        }
                    }
                }
                return false;
            }
        }
        catch(IOException e)
        {
            return false;
        }
    }

    public static void setBoolean(String dataLocation, String fileName, String bool, boolean value)
    {
        try
        {
            File dir = new File(IOUtils.getCurrentLocation() + File.separator + "mods" + File.separator + dataLocation);
            dir.mkdirs();
            File file = new File(dir.getAbsolutePath() + File.separator + fileName);
            if(!file.exists())
            {
                file.createNewFile();
                return;
            }
            if(!file.canWrite())
            {
                if(!file.setWritable(true))
                {
                    InfernoCore.log.warn("InfernoCore was denied write permission for file '" + fileName + "'.");
                    return;
                }
            }
            clearFile(file);
            try(FileWriter fw = new FileWriter(file); BufferedWriter writer = new BufferedWriter(fw))
            {
                writer.write(bool + "=" + value);
                writer.close();
            }
        }
        catch(IOException e)
        {
            InfernoCore.log.warn("Failed to set boolean '" + bool + "' to value '" + value + "' in file '" + fileName + "'.");
            e.printStackTrace();
        }
    }

    public static void clearFile(File file) throws IOException
    {
        file.delete();
        file.createNewFile();
    }

    public static boolean isValidInt(String s)
    {
        try
        {
            Integer.parseInt(s);
            return true;
        }
        catch(NumberFormatException e)
        {
            return false;
        }
    }
}
