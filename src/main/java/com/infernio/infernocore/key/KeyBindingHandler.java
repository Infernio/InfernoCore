package com.infernio.infernocore.key;

import net.minecraftforge.common.MinecraftForge;

public class KeyBindingHandler
{
    public static void init()
    {
        MinecraftForge.EVENT_BUS.register(new KeyRegistry());
    }
}
