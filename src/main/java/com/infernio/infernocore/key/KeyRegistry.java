package com.infernio.infernocore.key;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.infernio.infernocore.network.PacketHelper;
import com.infernio.infernocore.network.message.MessageKeyPressed;
import com.infernio.infernocore.network.message.MessageKeyPressed.KeyBindingType;

import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class KeyRegistry
{
    private static HashMap<String, KeyBinding> keyBindings = new HashMap<>();
    private static HashMap<String, ArrayList<IKeyTrigger>> keyTriggers = new HashMap<>();

    public static void registerKeyBinding(KeyBinding kb)
    {
        if(!keyBindings.containsKey(kb.getKeyDescription()))
        {
            keyBindings.put(kb.getKeyDescription(), kb);
        }
    }

    public static void registerKeyTrigger(String key, IKeyTrigger trigger)
    {
        if(!keyTriggers.containsKey(key))
        {
            ArrayList<IKeyTrigger> triggers = new ArrayList<>();
            triggers.add(trigger);
            keyTriggers.put(key, triggers);
        }
        else
        {
            keyTriggers.get(key).add(trigger);
        }
    }

    public static KeyBinding getKeyBinding(String keyDescription)
    {
        return keyBindings.get(keyDescription);
    }

    public static List<IKeyTrigger> getTrigger(String key)
    {
        return keyTriggers.get(key);
    }

    private static KeyBinding getPressedKeybinding()
    {
        for(KeyBinding kb : keyBindings.values())
        {
            if(kb != null && kb.isPressed())
            {
                return kb;
            }
        }
        return null;
    }

    @SubscribeEvent
    public void handleKeyInput(InputEvent.KeyInputEvent event)
    {
        KeyBinding kb = getPressedKeybinding();
        if(kb != null)
        {
            EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
            if(player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if(stack != null && stack.getItem() instanceof IKeyBoundItem)
                {
                    handleKeyInputItem(player, kb, stack);
                }
                else
                {
                    stack = player.getHeldItemOffhand();
                    if(stack != null && stack.getItem() instanceof IKeyBoundItem)
                    {
                        handleKeyInputItem(player, kb, stack);
                    }
                    else
                    {
                        List<IKeyTrigger> triggers = keyTriggers.get(kb.getKeyDescription());
                        if(triggers != null && !triggers.isEmpty())
                        {
                            for(IKeyTrigger trigger : triggers)
                            {
                                if(player.world.isRemote)
                                {
                                    PacketHelper.sendToServer(new MessageKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), KeyBindingType.TRIGGER));
                                }
                                else
                                {
                                    trigger.onKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), player);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void handleKeyInputItem(EntityPlayer player, KeyBinding kb, ItemStack stack)
    {
        if(player.world.isRemote)
        {
            PacketHelper.sendToServer(new MessageKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), KeyBindingType.ITEM));
        }
        else
        {
            ((IKeyBoundItem)stack.getItem()).onKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), player, stack);
        }
    }
}
