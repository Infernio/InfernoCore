package com.infernio.infernocore.key;

import net.minecraft.entity.player.EntityPlayer;

public interface IKeyTrigger
{
    void onKeyPressed(String description, int keyCode, String category, EntityPlayer player);
}
