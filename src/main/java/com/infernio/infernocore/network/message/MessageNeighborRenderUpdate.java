package com.infernio.infernocore.network.message;

import com.infernio.infernocore.InfernoCore;
import com.infernio.infernocore.world.WorldUtils;

import io.netty.buffer.ByteBuf;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageNeighborRenderUpdate implements IMessage, IMessageHandler<MessageNeighborRenderUpdate, IMessage>
{
    private int x;
    private int y;
    private int z;

    public MessageNeighborRenderUpdate() {}

    public MessageNeighborRenderUpdate(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
    }

    @Override
    public IMessage onMessage(MessageNeighborRenderUpdate message, MessageContext ctx)
    {
        World[] worlds = InfernoCore.proxy.getCurrentWorlds();
        for(World world : worlds)
        {
            WorldUtils.markNeighborsRenderUpdate(world, message.x, message.y, message.z);
        }
        return null;
    }
}
