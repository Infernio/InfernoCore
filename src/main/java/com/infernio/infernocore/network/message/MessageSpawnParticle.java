package com.infernio.infernocore.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.EnumParticleTypes;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSpawnParticle implements IMessage, IMessageHandler<MessageSpawnParticle, IMessage>
{
    private double x;
    private double y;
    private double z;
    private double velX;
    private double velY;
    private double velZ;
    private int particle;
    private int[] parameters;

    public MessageSpawnParticle() {}

    public MessageSpawnParticle(double x, double y, double z, double velX, double velY, double velZ, EnumParticleTypes particle, int... params)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.velX = velX;
        this.velY = velY;
        this.velZ = velZ;
        this.particle = particle.ordinal();
        this.parameters = params;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readDouble();
        this.y = buf.readDouble();
        this.z = buf.readDouble();
        this.velX = buf.readDouble();
        this.velY = buf.readDouble();
        this.velZ = buf.readDouble();
        this.particle = buf.readInt();
        int length = buf.readInt();
        this.parameters = new int[length];
        for(int i = 0; i < length; ++i)
        {
            this.parameters[i] = buf.readInt();
        }
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeDouble(this.x);
        buf.writeDouble(this.y);
        buf.writeDouble(this.z);
        buf.writeDouble(this.velX);
        buf.writeDouble(this.velY);
        buf.writeDouble(this.velZ);
        buf.writeInt(this.particle);
        buf.writeInt(this.parameters.length);
        for(int param : this.parameters)
        {
            buf.writeInt(param);
        }
    }

    @Override
    public IMessage onMessage(MessageSpawnParticle message, MessageContext ctx)
    {
        FMLClientHandler.instance().getWorldClient().spawnParticle(EnumParticleTypes.values()[this.particle], message.x, message.y, message.z, message.velX, message.velY, message.velZ, message.parameters);
        return null;
    }
}
