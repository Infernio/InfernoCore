package com.infernio.infernocore.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessagePlayEvent implements IMessage, IMessageHandler<MessagePlayEvent, IMessage>
{
    private int x;
    private int y;
    private int z;
    private int eventID;
    private int data;

    public MessagePlayEvent() {}

    public MessagePlayEvent(int x, int y, int z, int eventID, int data)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.eventID = eventID;
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.eventID = buf.readInt();
        this.data = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.eventID);
        buf.writeInt(this.data);
    }

    @Override
    public IMessage onMessage(MessagePlayEvent message, MessageContext ctx)
    {
        FMLClientHandler.instance().getWorldClient().playEvent(message.eventID, new BlockPos(message.x, message.y, message.z), message.data);
        return null;
    }
}
