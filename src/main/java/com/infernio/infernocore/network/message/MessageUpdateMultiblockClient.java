package com.infernio.infernocore.network.message;

import com.infernio.infernocore.InfernoCore;
import com.infernio.infernocore.multiblock.TileMultiblock;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageUpdateMultiblockClient implements IMessage, IMessageHandler<MessageUpdateMultiblockClient, IMessage>
{
    private int x;
    private int y;
    private int z;
    private boolean completed;

    public MessageUpdateMultiblockClient() {}

    public MessageUpdateMultiblockClient(int x, int y, int z, boolean completed)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.completed = completed;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.completed = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeBoolean(this.completed);
    }

    @Override
    public IMessage onMessage(MessageUpdateMultiblockClient message, MessageContext ctx)
    {
        World[] worlds = InfernoCore.proxy.getCurrentWorlds();
        for(World world : worlds)
        {
            BlockPos pos = new BlockPos(message.x, message.y, message.z);
            if(world.isAreaLoaded(pos, 1, true))
            {
                TileEntity tile = world.getTileEntity(pos);
                if(tile instanceof TileMultiblock)
                {
                    ((TileMultiblock)tile).clientCompleted = message.completed;
                }
            }
        }
        return null;
    }
}
