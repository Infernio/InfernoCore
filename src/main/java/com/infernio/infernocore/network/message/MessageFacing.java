package com.infernio.infernocore.network.message;

import com.infernio.infernocore.InfernoCore;
import com.infernio.infernocore.tile.IRotatable;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageFacing implements IMessage, IMessageHandler<MessageFacing, IMessage>
{
    private int x;
    private int y;
    private int z;
    private byte facing;

    public MessageFacing() {}

    public MessageFacing(int x, int y, int z, byte facing)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.facing = facing;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.facing = buf.readByte();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeByte(this.facing);
    }

    @Override
    public IMessage onMessage(MessageFacing message, MessageContext ctx)
    {
        TileEntity tile = FMLClientHandler.instance().getWorldClient().getTileEntity(new BlockPos(message.x, message.y, message.z));
        if(tile instanceof IRotatable)
        {
            IRotatable tile2 = (IRotatable)tile;
            if(tile2.acceptsRotation())
            {
                tile2.rotate(message.facing);
            }
        }
        else
        {
            InfernoCore.log.warn("Attempted to rotate a tile entity that does not implement IRotatable.");
            InfernoCore.log.warn("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
