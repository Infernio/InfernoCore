package com.infernio.infernocore.network;

import com.infernio.infernocore.Resources;
import com.infernio.infernocore.network.message.*;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketHelper
{
    private static final SimpleNetworkWrapper wrapper = NetworkRegistry.INSTANCE.newSimpleChannel(Resources.CHANNEL);

    public static void init()
    {
        wrapper.registerMessage(MessageSpawnParticle.class, MessageSpawnParticle.class, Packets.PACKET_PARTICLE_SPAWN, Side.CLIENT);
        wrapper.registerMessage(MessagePlayEvent.class, MessagePlayEvent.class, Packets.PACKET_PLAY_EVENT, Side.CLIENT);
        wrapper.registerMessage(MessageNeighborRenderUpdate.class, MessageNeighborRenderUpdate.class, Packets.PACKET_RENDER_NEIGHBOR_UPDATE, Side.CLIENT);
        wrapper.registerMessage(MessageOpenGUI.class, MessageOpenGUI.class, Packets.PACKET_OPEN_GUI, Side.SERVER);
        wrapper.registerMessage(MessageUpdateMultiblockClient.class, MessageUpdateMultiblockClient.class, Packets.PACKET_UPDATE_MULTIBLOCK_CLIENT, Side.CLIENT);
        wrapper.registerMessage(MessageKeyPressed.class, MessageKeyPressed.class, Packets.PACKET_KEY_PRESSED, Side.SERVER);
        wrapper.registerMessage(MessageFacing.class, MessageFacing.class, Packets.PACKET_SET_CLIENT_FACING, Side.CLIENT);
    }

    public static void sendTo(IMessage message, EntityPlayerMP player)
    {
        wrapper.sendTo(message, player);
    }

    public static void sendToAll(IMessage message)
    {
        wrapper.sendToAll(message);
    }

    public static void sendToAllAround(IMessage message, int dimension, int x, int y, int z, int range)
    {
        sendToAllAround(message, new TargetPoint(dimension, x, y, z, range));
    }

    public static void sendToAllAround(IMessage message, TargetPoint point)
    {
        wrapper.sendToAllAround(message, point);
    }

    public static void sendToDimension(IMessage message, int dimension)
    {
        wrapper.sendToDimension(message, dimension);
    }

    public static void sendToServer(IMessage message)
    {
        wrapper.sendToServer(message);
    }
}
