package com.infernio.infernocore.network;

public class Packets
{
    public static int PACKET_SPAWN_PARTICLE_RANGE;
    public static int PACKET_PLAY_EVENT_RANGE;

    public static final byte PACKET_PARTICLE_SPAWN = 0;
    public static final byte PACKET_PLAY_EVENT = 1;
    public static final byte PACKET_RENDER_NEIGHBOR_UPDATE = 2;
    public static final byte PACKET_OPEN_GUI = 3;
    public static final byte PACKET_UPDATE_MULTIBLOCK_CLIENT = 4;
    public static final byte PACKET_KEY_PRESSED = 5;
    public static final byte PACKET_SET_CLIENT_FACING = 6;
}
