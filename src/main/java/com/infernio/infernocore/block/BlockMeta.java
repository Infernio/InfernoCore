package com.infernio.infernocore.block;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public abstract class BlockMeta extends Block
{
    public final int maxMeta;

    public BlockMeta(Material material, int maxMeta)
    {
        super(material);
        this.maxMeta = maxMeta;
        this.setDefaultState(this.getDefaultState().withProperty(this.getMetadataProperty(), 0));
    }

    @Override
    public int damageDropped(IBlockState state)
    {
        return this.getMetaFromState(state);
    }

    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(this.getMetadataProperty(), meta);
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        return state.getValue(this.getMetadataProperty());
    }

    @Override
    public void getSubBlocks(CreativeTabs tab, NonNullList<ItemStack> list)
    {
        for(int i = 0; i < this.maxMeta; ++i)
        {
            list.add(new ItemStack(this, 1, i));
        }
    }

    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, this.getMetadataProperty());
    }

    public abstract IProperty<Integer> getMetadataProperty();
}
