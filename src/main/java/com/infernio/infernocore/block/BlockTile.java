package com.infernio.infernocore.block;

import java.util.Random;

import com.infernio.infernocore.tile.TileBasicInventory;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class BlockTile extends BlockContainer
{
    protected Random rand = new Random();

    public BlockTile(Material material)
    {
        super(material);
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase entity, ItemStack stack)
    {
        TileEntity tile = world.getTileEntity(pos);
        if(tile != null && tile instanceof TileBasicInventory)
        {
            if(stack.hasDisplayName())
            {
                ((TileBasicInventory)tile).setInventoryName(stack.getDisplayName());
            }
        }
    }

    @Override
    @Deprecated
    public boolean hasTileEntity()
    {
        return true;
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        TileEntity tile = world.getTileEntity(pos);
        if(tile == null || !(tile instanceof TileBasicInventory))
        {
            return false;
        }
        if(player.isSneaking() && player.getHeldItemMainhand() != null)
        {
            return false;
        }
        this.openGUI(player, world, pos);
        return true;
    }

    public abstract void openGUI(EntityPlayer player, World world, BlockPos pos);

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state)
    {
        TileEntity tile = world.getTileEntity(pos);
        if(tile instanceof IInventory)
        {
            InventoryHelper.dropInventoryItems(world, pos, (IInventory)tile);
            world.updateComparatorOutputLevel(pos, state.getBlock());
        }
        super.breakBlock(world, pos, state);
    }
}
