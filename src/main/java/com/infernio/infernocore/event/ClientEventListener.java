package com.infernio.infernocore.event;

import com.infernio.infernocore.InfernoCore;
import com.infernio.infernocore.client.icon.TextureRegistry;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientEventListener
{
    @SubscribeEvent
    public void loadTextures(TextureStitchEvent.Pre event)
    {
        TextureRegistry.getInstance().loadTextures(event.getMap());
    }

    @SubscribeEvent
    public void postLoadTextures(TextureStitchEvent.Post event)
    {
        TextureRegistry.getInstance().postLoadTextures(event.getMap());
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void handleTooltips(ItemTooltipEvent event)
    {
        if(InfernoCore.showRawNames)
        {
            ResourceLocation loc = Item.REGISTRY.getNameForObject(event.getItemStack().getItem());
            if(loc != null)
            {
                event.getToolTip().add(TextFormatting.BLUE + loc.getResourceDomain());
                if(GuiScreen.isShiftKeyDown())
                {
                    event.getToolTip().add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + loc.getResourceDomain() + ":" + loc.getResourcePath());
                }
            }
        }
    }
}
