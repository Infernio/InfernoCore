package com.infernio.infernocore.tile;

public abstract class TileMachineInventory extends TileInventoryElectric
{
    protected boolean disabled;

    public TileMachineInventory(int size)
    {
        super(size);
        this.disabled = false;
    }

    @Override
    public void update()
    {
        super.update();
        if(!this.world.isRemote)
        {
            if(this.isRedstoneControlled())
            {
                this.checkRedstone();
                if(!this.disabled)
                {
                    this.fillBuffer();
                    this.onUpdate();
                }
            }
            else
            {
                this.fillBuffer();
                this.onUpdate();
            }
        }
    }

    private void checkRedstone()
    {
        if(this.world.isBlockIndirectlyGettingPowered(this.pos) > 0)
        {
            this.disabled = true;
            return;
        }
        this.disabled = false;
    }

    public boolean isDisabled()
    {
        return this.disabled;
    }

    public abstract void onUpdate();

    public abstract boolean isRedstoneControlled();
}
