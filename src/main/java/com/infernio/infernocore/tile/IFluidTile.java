package com.infernio.infernocore.tile;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;

public interface IFluidTile extends IFluidHandler
{
    FluidStack getFluid();

    void emptyTank();

    void fillTank();

    int getFluidAmount();

    void decreaseFluidAmount(int by);

    void increaseFluidAmount(int by);

    void setFluidAmount(int to);

    int getCapacity();

    void decreaseCapacity(int by);

    void increaseCapacity(int by);

    void setCapacity(int to);

    int getScaledAmount(int scaleBy);
}
