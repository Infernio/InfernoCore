package com.infernio.infernocore.tile;

import com.infernio.infernocore.util.NBTLib;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.translation.I18n;

public abstract class TileBasicInventory extends TileEntity implements ISidedInventory
{
    public static final int STACK_LIMIT = 64;

    protected ItemStack[] inv;
    private int[] fullInventory;
    private String customName;

    public TileBasicInventory(int size)
    {
        this.inv = new ItemStack[size];
        this.fullInventory = new int[size];
        for(int i = 0; i < size; ++i)
        {
            this.fullInventory[i] = i;
        }
        this.customName = null;
    }

    @Override
    public int getSizeInventory()
    {
        return this.inv.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return this.inv[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount)
    {
        ItemStack stack = this.getStackInSlot(slot);
        if(stack != null)
        {
            if(stack.getCount() <= amount)
            {
                this.setInventorySlotContents(slot, null);
            }
            else
            {
                stack = stack.splitStack(amount);
                if(stack.getCount() <= 0)
                {
                    this.setInventorySlotContents(slot, null);
                }
            }
        }
        this.markDirty();
        return stack;
    }

    @Override
    public ItemStack removeStackFromSlot(int slot)
    {
        ItemStack ret = this.inv[slot];
        this.setInventorySlotContents(slot, null);
        return ret;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        this.inv[slot] = stack;
        this.markDirty();
        if(stack != null && stack.getCount() > this.getInventoryStackLimit())
        {
            stack.setCount(this.getInventoryStackLimit());
        }
    }

    @Override
    public String getName()
    {
        return this.hasCustomName() ? this.customName : this.getDefaultName();
    }

    @Override
    public boolean hasCustomName()
    {
        return this.customName != null && !this.customName.isEmpty();
    }

    @Override
    public ITextComponent getDisplayName()
    {
        return new TextComponentString(I18n.translateToLocal(this.getName()));
    }

    @Override
    public int getInventoryStackLimit()
    {
        return STACK_LIMIT;
    }

//    @Override
//    public boolean isUseableByPlayer(EntityPlayer player)
//    {
//        return this.world.getTileEntity(this.pos) != this ? false : player.getDistanceSq(this.pos.getX() + 0.5D, this.pos.getY() + 0.5D, this.pos.getZ() + 0.5D) <= 64.0D;
//    }

    @Override
    public void openInventory(EntityPlayer player) {}

    @Override
    public void closeInventory(EntityPlayer player) {}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return true;
    }

    @Override
    public int getField(int id)
    {
        return 0;
    }

    @Override
    public void setField(int id, int value) {}

    @Override
    public int getFieldCount()
    {
        return 0;
    }

    @Override
    public void clear()
    {
        for(int i = 0; i < this.inv.length; ++i)
        {
            this.inv[i] = null;
        }
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack stack, EnumFacing direction)
    {
        return direction != EnumFacing.DOWN;
    }

    @Override
    public boolean canExtractItem(int slot, ItemStack stack, EnumFacing direction)
    {
        return direction == EnumFacing.DOWN;
    }

    @Override
    public int[] getSlotsForFace(EnumFacing side)
    {
        return this.fullInventory;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        NBTTagList nbttaglist = tag.getTagList("Inventory", NBTLib.NBT_COMPOUND);
        this.inv = new ItemStack[this.getSizeInventory()];
        for(int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound tag2 = nbttaglist.getCompoundTagAt(i);
            byte b = tag2.getByte("Slot");
            if(b >= 0 && b < this.inv.length)
            {
                this.inv[b] = new ItemStack(tag2);
            }
        }
        if(tag.hasKey("CustomName"))
        {
            this.customName = tag.getString("CustomName");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        NBTTagList tagList = new NBTTagList();
        for(int i = 0; i < this.inv.length; ++i)
        {
            if(this.inv[i] != null)
            {
                NBTTagCompound tag2 = new NBTTagCompound();
                tag2.setByte("Slot", (byte)i);
                this.inv[i].writeToNBT(tag2);
                tagList.appendTag(tag2);
            }
        }
        tag.setTag("Inventory", tagList);
        if(this.hasCustomName())
        {
            tag.setString("CustomName", this.customName);
        }
        return tag;
    }

    public void setInventoryName(String to)
    {
        this.customName = to;
    }

    public abstract String getDefaultName();
}
