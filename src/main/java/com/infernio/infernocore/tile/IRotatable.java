package com.infernio.infernocore.tile;

// TODO Rework maybe?
public interface IRotatable
{
    boolean acceptsRotation();

    void rotate(int facing);

    int getFacing();
}
