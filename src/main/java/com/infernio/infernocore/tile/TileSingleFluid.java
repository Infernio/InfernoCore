package com.infernio.infernocore.tile;

import com.infernio.infernocore.math.MathUtils;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fluids.capability.TileFluidHandler;

public abstract class TileSingleFluid extends TileFluidHandler implements IFluidTile
{
    public TileSingleFluid(int capacity)
    {
        this(null, capacity);
    }

    public TileSingleFluid(Fluid fluid, int amount, int capacity)
    {
        this(new FluidStack(fluid, amount), capacity);
    }

    public TileSingleFluid(FluidStack fluid, int capacity)
    {
        this.tank = new FluidTank(fluid, capacity);
    }

    @Override
    public FluidStack getFluid()
    {
        return this.tank.getFluid();
    }

    @Override
    public void emptyTank()
    {
        this.tank.setFluid(null);
    }

    @Override
    public void fillTank()
    {
        this.tank.getFluid().amount = this.tank.getCapacity();
    }

    @Override
    public int getFluidAmount()
    {
        return this.tank.getFluidAmount();
    }

    @Override
    public void decreaseFluidAmount(int by)
    {
        if(this.getFluidAmount() - by <= 0)
        {
            this.emptyTank();
        }
        else
        {
            this.getFluid().amount -= by;
        }
    }

    @Override
    public void increaseFluidAmount(int by)
    {
        if(this.getFluidAmount() + by > this.getCapacity())
        {
            this.fillTank();
        }
        else
        {
            this.getFluid().amount += by;
        }
    }

    @Override
    public void setFluidAmount(int to)
    {
        this.tank.getFluid().amount = MathHelper.clamp(to, 0, this.getCapacity());
    }

    @Override
    public int getCapacity()
    {
        return this.tank.getCapacity();
    }

    @Override
    public void decreaseCapacity(int by)
    {
        if(this.getCapacity() - by < 0)
        {
            this.setCapacity(0);
        }
        else
        {
            this.tank.setCapacity(this.getCapacity() - by);
        }
    }

    @Override
    public void increaseCapacity(int by)
    {
        if(this.getCapacity() + by < 0)
        {
            this.setCapacity(0);
        }
        else
        {
            this.tank.setCapacity(this.getCapacity() + by);
        }
    }

    @Override
    public void setCapacity(int to)
    {
        this.tank.setCapacity(MathUtils.ensurePositive(to));
    }

    @Override
    public int getScaledAmount(int scaleBy)
    {
        return (int)(((float)this.getFluidAmount() / (float)this.getCapacity()) * scaleBy);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.tank.readFromNBT(tag.getCompoundTag("Fluid"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setTag("Fluid", new NBTTagCompound());
        this.tank.writeToNBT(tag.getCompoundTag("Fluid"));
        return tag;
    }

    @Override
    public int fill(FluidStack stack, boolean doFill)
    {
        return this.tank.fill(stack, doFill);
    }

    @Override
    public FluidStack drain(FluidStack stack, boolean doDrain)
    {
        if(stack.isFluidEqual(this.tank.getFluid()))
        {
            return this.tank.drain(stack.amount, doDrain);
        }
        return null;
    }

    @Override
    public FluidStack drain(int maxDrain, boolean doDrain)
    {
        return this.tank.drain(maxDrain, doDrain);
    }

    @Override
    public IFluidTankProperties[] getTankProperties()
    {
        return this.tank.getTankProperties();
    }
}
