package com.infernio.infernocore.tile;

public abstract class TileMachine extends TileElectric
{
    protected boolean disabled;

    public TileMachine()
    {
        this.disabled = false;
    }

    @Override
    public void update()
    {
        super.update();
        if(!this.world.isRemote)
        {
            if(this.isRedstoneControlled())
            {
                this.checkRedstone();
                if(!this.disabled)
                {
                    this.fillBuffer();
                    this.onUpdate();
                }
            }
            else
            {
                this.fillBuffer();
                this.onUpdate();
            }
        }
    }

    private void checkRedstone()
    {
        if(this.world.isBlockIndirectlyGettingPowered(this.pos) > 0)
        {
            this.disabled = true;
            return;
        }
        this.disabled = false;
    }

    public abstract void onUpdate();

    public abstract boolean isRedstoneControlled();
}
