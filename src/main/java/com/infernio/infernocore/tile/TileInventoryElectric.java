package com.infernio.infernocore.tile;

// import ic2.api.energy.event.EnergyTileLoadEvent;
// import ic2.api.energy.event.EnergyTileUnloadEvent;
// import ic2.api.energy.tile.IEnergySink;
import net.minecraft.nbt.NBTTagCompound;
// import net.minecraft.util.Direction;
import net.minecraft.util.ITickable;

// TODO Get IC2 and BC support back
public abstract class TileInventoryElectric extends TileBasicInventory implements ITickable //, IPowerReceptor, IEnergySink {
{
    public int energy;
    private boolean firstTick;
    //private PowerHandler handler;

    public TileInventoryElectric(int size)
    {
        super(size);
        this.firstTick = true;
        //    	this.handler = new PowerHandler(this, PowerHandler.Type.MACHINE);
        //    	this.handler.configure(0.0F, 50.0F, 0.0F, this.getMaxEnergy() / 5.0F);
        //    	this.handler.configurePowerPerdition(1, 1);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.energy = tag.getShort("Energy");
        //        this.handler.readFromNBT(tag, "BCPower");
        //        this.handler.configure(0.0F, 50.0F, 0.0F, this.getMaxEnergy() / 5.0F);
        //        this.handler.configurePowerPerdition(1, 1);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Energy", (short)this.energy);
        //        this.handler.writeToNBT(tag, "BCPower");
        return tag;
    }

    @Override
    public void onChunkUnload()
    {
        super.onChunkUnload();
        //    	if(!this.worldObj.isRemote)
        //    	{
        //	    	MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
        //	    }
    }

    @Override
    public void invalidate()
    {
        super.invalidate();
        //    	if(!this.worldObj.isRemote)
        //    	{
        //	    	MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
        //	    }
    }

    public void fillBuffer()
    {
        //    	int maxEnergy = this.getMaxEnergy();
        //    	if(this.energy < maxEnergy)
        //		{
        //	     	double energyRequired = handler.useEnergy(0.0F, 25.0F, true);
        //	    	int converted = (int)(energyRequired * 5);
        //			int newAmount = this.energy + converted;
        //			if(newAmount >= maxEnergy)
        //			{
        //				this.energy = maxEnergy;
        //			}
        //			else
        //			{
        //				this.energy += converted;
        //		    }
        //		}
    }

    //	@Override
    //	public PowerReceiver getPowerReceiver(ForgeDirection side)
    //	{
    //		return this.handler.getPowerReceiver();
    //	}

    //	@Override
    //	public void doWork(PowerHandler workProvider) {}

    //	@Override
    //	public int demandsEnergy()
    //	{
    //		if(this.energy < this.getMaxEnergy())
    //		{
    //			return this.getEUScale();
    //		}
    //		return 0;
    //	}

    //	@Override
    //	public int injectEnergy(Direction directionFrom, int amount)
    //	{
    //		int maxEnergy = this.getMaxEnergy();
    //		if(this.energy < maxEnergy)
    //		{
    //			int newAmount = (int)(this.energy + amount);
    //			if(newAmount >= maxEnergy)
    //			{
    //				this.energy = maxEnergy;
    //				return newAmount - maxEnergy;
    //			}
    //			this.energy += amount;
    //			return 0;
    //		}
    //		return amount;
    //	}

    //	@Override
    //	public int getMaxSafeInput()
    //	{
    //		return this.getEUScale();
    //	}

    //	@Override
    //	public boolean acceptsEnergyFrom(TileEntity emitter, Direction direction)
    //	{
    //		return true;
    //	}

    //	@Override
    //	public boolean isAddedToEnergyNet()
    //	{
    //		return !this.firstTick;
    //	}

    public abstract int getMaxEnergy();

    public abstract int getEUScale();

    @Override
    public void update()
    {
        if(this.firstTick)
        {
            //	    	if(!this.worldObj.isRemote)
            //	    	{
            //	    		MinecraftForge.EVENT_BUS.post(new EnergyTileLoadEvent(this));
            //	    	}
            this.firstTick = false;
        }
    }
}
