package com.infernio.infernocore.collect;

public class MapFilter<K, V> implements IMapFilter<K, V> {

	private IFilter<K> keyFilter;
	private IFilter<V> valueFilter;

	public MapFilter(IFilter<K> keyFilter, IFilter<V> valueFilter)
	{
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	@Override
	public boolean acceptKey(K key)
	{
		return this.keyFilter.accept(key);
	}

	@Override
	public boolean acceptValue(V value)
	{
		return this.valueFilter.accept(value);
	}

}