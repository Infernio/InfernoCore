package com.infernio.infernocore.collect;

public final class LogicFilter {

	public static final class And<E> implements IFilter<E> {

		private IFilter<E> filter1;
		private IFilter<E> filter2;

		public And(IFilter<E> filter1, IFilter<E> filter2)
		{
			this.filter1 = filter1;
			this.filter2 = filter2;
		}

		@Override
		public boolean accept(E element)
		{
			return this.filter1.accept(element) && this.filter2.accept(element);
		}

	}

	public static final class Or<E> implements IFilter<E> {

		private IFilter<E> filter1;
		private IFilter<E> filter2;

		public Or(IFilter<E> filter1, IFilter<E> filter2)
		{
			this.filter1 = filter1;
			this.filter2 = filter2;
		}

		@Override
		public boolean accept(E element)
		{
			return this.filter1.accept(element) || this.filter2.accept(element);
		}

	}

	public static final class XOR<E> implements IFilter<E> {

		private IFilter<E> filter1;
		private IFilter<E> filter2;

		public XOR(IFilter<E> filter1, IFilter<E> filter2)
		{
			this.filter1 = filter1;
			this.filter2 = filter2;
		}

		@Override
		public boolean accept(E element)
		{
			return this.filter1.accept(element) ^ this.filter2.accept(element);
		}

	}

	public static final class Negate<E> implements IFilter<E> {

		private IFilter<E> filter;

		public Negate(IFilter<E> filter)
		{
			this.filter = filter;
		}

		@Override
		public boolean accept(E element)
		{
			return !this.filter.accept(element);
		}

	}

}
