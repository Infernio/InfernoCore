package com.infernio.infernocore.collect;

public interface IFilter<E> {

	boolean accept(E element);

}
