package com.infernio.infernocore.collect;

import java.util.ArrayList;
import java.util.Collection;

public class FilteredArrayList<E> extends ArrayList<E> implements IFilteredList<E> {

	private static final long serialVersionUID = 1L;

	private IFilter<E> filter;

	public FilteredArrayList(IFilter<E> filter)
	{
		this.filter = filter;
	}

	public FilteredArrayList(IFilter<E> filter, int capacity)
	{
		super(capacity);
		this.filter = filter;
	}

	public FilteredArrayList(IFilter<E> filter, Collection<? extends E> collection)
	{
		super(collection);
		this.filter = filter;
	}

	@Override
	public boolean add(E element)
	{
		if(this.filter.accept(element))
		{
			return super.add(element);
		}
		return false;
	}

	@Override
	public void add(int index, E element)
	{
		if(this.filter.accept(element))
		{
			super.add(index, element);
		}
	}

	@Override
	public boolean addAll(Collection<? extends E> collection)
	{
		ArrayList<E> temp = new ArrayList<>();
		for(E element : collection)
		{
			if(this.filter.accept(element))
			{
				temp.add(element);
			}
		}
		return super.addAll(temp);
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> collection)
	{
		ArrayList<E> temp = new ArrayList<>();
		for(E element : collection)
		{
			if(this.filter.accept(element))
			{
				temp.add(element);
			}
		}
		return super.addAll(index, temp);
	}

	@Override
	public E set(int index, E element)
	{
		if(this.filter.accept(element))
		{
			return super.set(index, element);
		}
		return null;
	}

}
