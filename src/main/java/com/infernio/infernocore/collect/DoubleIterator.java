package com.infernio.infernocore.collect;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.minecraft.nbt.NBTTagCompound;

public class DoubleIterator<E1, E2> {

	private List<E1> firstList;
	private List<E2> secondList;
	private int position;
	private int size;
	private boolean finished;
	private boolean firstCalled;
	private boolean secondCalled;
	private boolean repeated;
	private int loops;
	private int timesLooped;

	public DoubleIterator(List<E1> firstList, List<E2> secondList, boolean repeated, int loops)
	{
		if(firstList.size() != secondList.size())
		{
			throw new IllegalArgumentException("[DoubledIterator] Invalid size: First list size was not equal to second list size!");
		}
		if(firstList.isEmpty())
		{
			throw new IllegalArgumentException("[DoubledIterator] List to iterate on was empty.");
		}
		this.size = firstList.size();
		this.firstList = firstList;
		this.secondList = secondList;
		this.loops = loops;
		this.repeated = repeated;
	}

	public DoubleIterator(Map<E1, E2> map, boolean repeated, int loops)
	{
		if(map.isEmpty())
		{
			throw new IllegalArgumentException("[DoubledIterator] Map to iterate on was empty.");
		}
		this.size = map.size();
		this.firstList = new ArrayList<>(map.keySet());
		this.secondList = new ArrayList<>(map.values());
		this.loops = loops;
		this.repeated = repeated;
	}

	public E1 nextFirst()
	{
		if(!this.firstCalled)
		{
			this.firstCalled = true;
		}
		E1 ret;
		if(this.firstCalled && this.secondCalled)
		{
			this.firstCalled = false;
			this.secondCalled = false;
			if(this.position >= this.size)
			{
				if(!this.repeated)
				{
					if(this.loops == -1)
					{
						this.finished = true;
					}
					else if(this.timesLooped < this.loops)
					{
						++this.timesLooped;
					}
					else
					{
						this.finished = true;
					}
				}
				this.position = 0;
			}
			ret = this.firstList.get(this.position);
			++this.position;
		}
		else
		{
			if(this.position >= this.size)
			{
				if(!this.repeated)
				{
					if(this.loops == -1)
					{
						this.finished = true;
					}
					else if(this.timesLooped < this.loops)
					{
						++this.timesLooped;
					}
					else
					{
						this.finished = true;
					}
				}
				this.position = 0;
			}
			ret = this.firstList.get(this.position);
		}
		return ret;
	}

	public E2 nextSecond()
	{
		if(!this.secondCalled)
		{
			this.secondCalled = true;
		}
		E2 ret;
		if(this.firstCalled && this.secondCalled)
		{
			this.firstCalled = false;
			this.secondCalled = false;
			if(this.position >= this.size)
			{
				if(!this.repeated)
				{
					if(this.loops == -1)
					{
						this.finished = true;
					}
					else if(this.timesLooped < this.loops)
					{
						++this.timesLooped;
					}
					else
					{
						this.finished = true;
					}
				}
				this.position = 0;
			}
			ret = this.secondList.get(this.position);
			++this.position;
		}
		else
		{
			if(this.position >= this.size)
			{
				if(!this.repeated)
				{
					if(this.loops == -1)
					{
						this.finished = true;
					}
					else if(this.timesLooped < this.loops)
					{
						++this.timesLooped;
					}
					else
					{
						this.finished = true;
					}
				}
				this.position = 0;
			}
			ret = this.secondList.get(this.position);
		}
		return ret;
	}

	public boolean isFinished()
	{
		return this.finished;
	}

	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setInteger("Position", this.position);
		tag.setInteger("TimesLooped", this.timesLooped);
		tag.setBoolean("SecondCalled", this.secondCalled);
		tag.setBoolean("FirstCalled", this.firstCalled);
		tag.setBoolean("Finished", this.finished);
	}

	public void readFromNBT(NBTTagCompound tag)
	{
		this.position = tag.getInteger("Position");
		this.timesLooped = tag.getInteger("TimesLooped");
		this.secondCalled = tag.getBoolean("FirstCalled");
		this.firstCalled = tag.getBoolean("SecondCalled");
		this.finished = tag.equals("Finished");
	}

}