package com.infernio.infernocore.collect;

import java.util.*;
import java.util.Map.Entry;

public class FilterUtils {

	public static <E> ArrayList<E> applyFilter(IFilter<E> filter, ArrayList<E> list)
	{
		ArrayList<E> ret = new ArrayList<>();
		for(E element : list)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> LinkedList<E> applyFilter(IFilter<E> filter, LinkedList<E> list)
	{
		LinkedList<E> ret = new LinkedList<>();
		for(E element : list)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> TreeSet<E> applyFilter(IFilter<E> filter, TreeSet<E> set)
	{
		TreeSet<E> ret = new TreeSet<>();
		for(E element : set)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> HashSet<E> applyFilter(IFilter<E> filter, HashSet<E> set)
	{
		HashSet<E> ret = new HashSet<>();
		for(E element : set)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> LinkedHashSet<E> applyFilter(IFilter<E> filter, LinkedHashSet<E> set)
	{
		LinkedHashSet<E> ret = new LinkedHashSet<>();
		for(E element : set)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> FilteredHashSet<E> applyFilter(IFilter<E> filter, FilteredHashSet<E> set)
	{
		FilteredHashSet<E> ret = new FilteredHashSet<>(filter);
		for(E element : set)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> FilteredLinkedHashSet<E> applyFilter(IFilter<E> filter, FilteredLinkedHashSet<E> set)
	{
		FilteredLinkedHashSet<E> ret = new FilteredLinkedHashSet<>(filter);
		for(E element : set)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> FilteredTreeSet<E> applyFilter(IFilter<E> filter, FilteredTreeSet<E> set)
	{
		FilteredTreeSet<E> ret = new FilteredTreeSet<>(filter);
		for(E element : set)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> HashSet<E> applyFilter(IFilter<E> filter, Set<E> set)
	{
		HashSet<E> ret = new HashSet<>();
		for(E element : set)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> FilteredHashSet<E> applyFilter(IFilter<E> filter, IFilteredSet<E> set)
	{
		FilteredHashSet<E> ret = new FilteredHashSet<>(filter);
		for(E element : set)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> FilteredArrayList<E> applyFilter(IFilter<E> filter, FilteredArrayList<E> list)
	{
		FilteredArrayList<E> ret = new FilteredArrayList<>(filter);
		for(E element : list)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> FilteredLinkedList<E> applyFilter(IFilter<E> filter, FilteredLinkedList<E> list)
	{
		FilteredLinkedList<E> ret = new FilteredLinkedList<>(filter);
		for(E element : list)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> FilteredArrayList<E> applyFilter(IFilter<E> filter, IFilteredList<E> list)
	{
		FilteredArrayList<E> ret = new FilteredArrayList<>(filter);
		for(E element : list)
		{
			ret.add(element);
		}
		return ret;
	}

	public static <E> ArrayList<E> applyFilter(IFilter<E> filter, List<E> list)
	{
		ArrayList<E> ret = new ArrayList<>();
		for(E element : list)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret;
	}

	public static <E> Object[] applyFilter(IFilter<E> filter, E[] array)
	{
		ArrayList<E> ret = new ArrayList<>();
		for(E element : array)
		{
			if(filter.accept(element))
			{
				ret.add(element);
			}
		}
		return ret.toArray();
	}

	public static <K, V> HashMap<K, V> applyFilter(IMapFilter<K, V> filter, HashMap<K, V> map)
	{
		HashMap<K, V> ret = new HashMap<>();
		for(Entry<K, V> entry : map.entrySet())
		{
			K key = entry.getKey();
			V value = entry.getValue();
			if(filter.acceptKey(key) && filter.acceptValue(value))
			{
				ret.put(key, value);
			}
		}
		return ret;
	}

	public static <K, V> TreeMap<K, V> applyFilter(IMapFilter<K, V> filter, TreeMap<K, V> map)
	{
		TreeMap<K, V> ret = new TreeMap<>();
		for(Entry<K, V> entry : map.entrySet())
		{
			K key = entry.getKey();
			V value = entry.getValue();
			if(filter.acceptKey(key) && filter.acceptValue(value))
			{
				ret.put(key, value);
			}
		}
		return ret;
	}

	public static <K, V> LinkedHashMap<K, V> applyFilter(IMapFilter<K, V> filter, LinkedHashMap<K, V> map)
	{
		LinkedHashMap<K, V> ret = new LinkedHashMap<>();
		for(Entry<K, V> entry : map.entrySet())
		{
			K key = entry.getKey();
			V value = entry.getValue();
			if(filter.acceptKey(key) && filter.acceptValue(value))
			{
				ret.put(key, value);
			}
		}
		return ret;
	}

	public static <K, V> FilteredHashMap<K, V> applyFilter(IMapFilter<K, V> filter, FilteredHashMap<K, V> map)
	{
		FilteredHashMap<K, V> ret = new FilteredHashMap<>(filter);
		for(Entry<K, V> entry : map.entrySet())
		{
			ret.put(entry.getKey(), entry.getValue());
		}
		return ret;
	}

	public static <K, V> FilteredLinkedHashMap<K, V> applyFilter(IMapFilter<K, V> filter, FilteredLinkedHashMap<K, V> map)
	{
		FilteredLinkedHashMap<K, V> ret = new FilteredLinkedHashMap<>(filter);
		for(Entry<K, V> entry : map.entrySet())
		{
			ret.put(entry.getKey(), entry.getValue());
		}
		return ret;
	}

	public static <K, V> FilteredTreeMap<K, V> applyFilter(IMapFilter<K, V> filter, FilteredTreeMap<K, V> map)
	{
		FilteredTreeMap<K, V> ret = new FilteredTreeMap<>(filter);
		for(Entry<K, V> entry : map.entrySet())
		{
			ret.put(entry.getKey(), entry.getValue());
		}
		return ret;
	}

	public static <K, V> FilteredHashMap<K, V> applyFilter(IMapFilter<K, V> filter, IFilteredMap<K, V> map)
	{
		FilteredHashMap<K, V> ret = new FilteredHashMap<>(filter);
		for(Entry<K, V> entry : map.entrySet())
		{
			ret.put(entry.getKey(), entry.getValue());
		}
		return ret;
	}

	public static <K, V> HashMap<K, V> applyFilter(IMapFilter<K, V> filter, Map<K, V> map)
	{
		HashMap<K, V> ret = new HashMap<>();
		for(Entry<K, V> entry : map.entrySet())
		{
			K key = entry.getKey();
			V value = entry.getValue();
			if(filter.acceptKey(key) && filter.acceptValue(value))
			{
				ret.put(key, value);
			}
		}
		return ret;
	}

}
