package com.infernio.infernocore.collect;

import java.util.Map;

public interface IFilteredMap<K, V> extends Map<K, V> {

}
