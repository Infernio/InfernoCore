package com.infernio.infernocore.collect;

public interface IMapFilter<K, V> {

	boolean acceptKey(K key);

	boolean acceptValue(V value);

}
