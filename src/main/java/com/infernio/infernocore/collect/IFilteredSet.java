package com.infernio.infernocore.collect;

import java.util.Set;

public interface IFilteredSet<E> extends Set<E> {

}
