package com.infernio.infernocore.collect;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class FilteredTreeMap<K, V> extends TreeMap<K, V> implements IFilteredMap<K, V> {

	private static final long serialVersionUID = 1L;

	private IFilter<K> keyFilter;
	private IFilter<V> valueFilter;
	private IMapFilter<K, V> mapFilter;

	public FilteredTreeMap(IFilter<K> keyFilter, IFilter<V> valueFilter)
	{
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredTreeMap(IMapFilter<K, V> mapFilter)
	{
		this.mapFilter = mapFilter;
	}

	public FilteredTreeMap(IMapFilter<K, V> mapFilter, Map<K, V> map)
	{
		super(map);
		this.mapFilter = mapFilter;
	}

	public FilteredTreeMap(IMapFilter<K, V> mapFilter, SortedMap<K, V> map)
	{
		super(map);
		this.mapFilter = mapFilter;
	}

	public FilteredTreeMap(IMapFilter<K, V> mapFilter, Comparator<? super K> comparator)
	{
		super(comparator);
		this.mapFilter = mapFilter;
	}

	public FilteredTreeMap(IFilter<K> keyFilter, IFilter<V> valueFilter, Map<K, V> map)
	{
		super(map);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredTreeMap(IFilter<K> keyFilter, IFilter<V> valueFilter, SortedMap<K, V> map)
	{
		super(map);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredTreeMap(IFilter<K> keyFilter, IFilter<V> valueFilter, Comparator<? super K> comparator)
	{
		super(comparator);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	@Override
	public V put(K key, V value)
	{
		if(this.mapFilter != null && this.mapFilter.acceptKey(key) && this.mapFilter.acceptValue(value))
		{
			return super.put(key, value);
		}
		if(this.keyFilter != null && this.valueFilter != null)
		{
			if(this.keyFilter.accept(key) && this.valueFilter.accept(value))
			{
				return super.put(key, value);
			}
		}
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> map)
	{
		Map<K, V> temp = new HashMap<>();
		for(Entry<? extends K, ? extends V> entry : map.entrySet())
		{
			K key = entry.getKey();
			V value = entry.getValue();
			if(this.mapFilter != null && this.mapFilter.acceptKey(key) && this.mapFilter.acceptValue(value))
			{
				temp.put(key, value);
				continue;
			}
			if(this.keyFilter != null && this.valueFilter != null)
			{
				if(this.keyFilter.accept(key) && this.valueFilter.accept(value))
				{
					temp.put(key, value);
				}
			}
		}
		super.putAll(temp);
	}
}
