package com.infernio.infernocore.collect;

import java.util.Collection;
import java.util.LinkedList;

public class FilteredLinkedList<E> extends LinkedList<E> implements IFilteredList<E> {

	private static final long serialVersionUID = 1L;

	private IFilter<E> filter;

	public FilteredLinkedList(IFilter<E> filter)
	{
		this.filter = filter;
	}

	public FilteredLinkedList(IFilter<E> filter, Collection<? extends E> collection)
	{
		super(collection);
		this.filter = filter;
	}

	@Override
	public boolean add(E element)
	{
		if(this.filter.accept(element))
		{
			return super.add(element);
		}
		return false;
	}

	@Override
	public void add(int index, E element)
	{
		if(this.filter.accept(element))
		{
			super.add(index, element);
		}
	}

	@Override
	public void addFirst(E element)
	{
		if(this.filter.accept(element))
		{
			super.addFirst(element);
		}
	}

	@Override
	public void addLast(E element)
	{
		if(this.filter.accept(element))
		{
			super.addLast(element);
		}
	}

	@Override
	public E set(int index, E element)
	{
		if(this.filter.accept(element))
		{
			return super.set(index, element);
		}
		return null;
	}

	@Override
	public boolean addAll(Collection<? extends E> collection)
	{
		LinkedList<E> temp = new LinkedList<>();
		for(E element : collection)
		{
			if(this.filter.accept(element))
			{
				temp.add(element);
			}
		}
		return super.addAll(temp);
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> collection)
	{
		LinkedList<E> temp = new LinkedList<>();
		for(E element : collection)
		{
			if(this.filter.accept(element))
			{
				temp.add(element);
			}
		}
		return super.addAll(index, temp);
	}

	@Override
	public boolean offer(E element)
	{
		if(this.filter.accept(element))
		{
			return super.offer(element);
		}
		return false;
	}

	@Override
	public boolean offerFirst(E element)
	{
		if(this.filter.accept(element))
		{
			return super.offerFirst(element);
		}
		return false;
	}

	@Override
	public boolean offerLast(E element)
	{
		if(this.filter.accept(element))
		{
			return super.offerLast(element);
		}
		return false;
	}

	@Override
	public void push(E element)
	{
		if(this.filter.accept(element))
		{
			super.push(element);
		}
	}

}
