package com.infernio.infernocore.collect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class FilteredHashSet<E> extends HashSet<E> implements IFilteredSet<E> {

	private static final long serialVersionUID = 1L;

	private IFilter<E> filter;

	public FilteredHashSet(IFilter<E> filter)
	{
		this.filter = filter;
	}

	public FilteredHashSet(IFilter<E> filter, int capacity)
	{
		super(capacity);
		this.filter = filter;
	}

	public FilteredHashSet(IFilter<E> filter, int capacity, float loadFactor)
	{
		super(capacity, loadFactor);
		this.filter = filter;
	}

	public FilteredHashSet(IFilter<E> filter, Collection<? extends E> collection)
	{
		super(collection);
		this.filter = filter;
	}

	@Override
	public boolean add(E element)
	{
		if(this.filter.accept(element))
		{
			return super.add(element);
		}
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends E> collection)
	{
		ArrayList<E> temp = new ArrayList<>();
		for(E element : collection)
		{
			if(this.filter.accept(element) && !temp.contains(element))
			{
				temp.add(element);
			}
		}
		return super.addAll(temp);
	}

}
