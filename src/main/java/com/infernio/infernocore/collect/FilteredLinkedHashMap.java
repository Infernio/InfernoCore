package com.infernio.infernocore.collect;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class FilteredLinkedHashMap<K, V> extends LinkedHashMap<K, V> implements IFilteredMap<K, V> {

	private static final long serialVersionUID = 1L;

	private IFilter<K> keyFilter;
	private IFilter<V> valueFilter;
	private IMapFilter<K, V> mapFilter;

	public FilteredLinkedHashMap(IFilter<K> keyFilter, IFilter<V> valueFilter)
	{
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredLinkedHashMap(IMapFilter<K, V> mapFilter)
	{
		this.mapFilter = mapFilter;
	}

	public FilteredLinkedHashMap(IMapFilter<K, V> mapFilter, int capacity)
	{
		super(capacity);
		this.mapFilter = mapFilter;
	}

	public FilteredLinkedHashMap(IMapFilter<K, V> mapFilter, int capacity, float loadFactor)
	{
		super(capacity, loadFactor);
		this.mapFilter = mapFilter;
	}

	public FilteredLinkedHashMap(IMapFilter<K, V> mapFilter, int capacity, float loadFactor, boolean accessOrder)
	{
		super(capacity, loadFactor, accessOrder);
		this.mapFilter = mapFilter;
	}

	public FilteredLinkedHashMap(IMapFilter<K, V> mapFilter, Map<K, V> map)
	{
		super(map);
		this.mapFilter = mapFilter;
	}

	public FilteredLinkedHashMap(IFilter<K> keyFilter, IFilter<V> valueFilter, int capacity)
	{
		super(capacity);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredLinkedHashMap(IFilter<K> keyFilter, IFilter<V> valueFilter, int capacity, float loadFactor)
	{
		super(capacity, loadFactor);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredLinkedHashMap(IFilter<K> keyFilter, IFilter<V> valueFilter, int capacity, float loadFactor, boolean accessOrder)
	{
		super(capacity, loadFactor, accessOrder);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	public FilteredLinkedHashMap(IFilter<K> keyFilter, IFilter<V> valueFilter, Map<K, V> map)
	{
		super(map);
		this.keyFilter = keyFilter;
		this.valueFilter = valueFilter;
	}

	@Override
	public V put(K key, V value)
	{
		if(this.mapFilter != null && this.mapFilter.acceptKey(key) && this.mapFilter.acceptValue(value))
		{
			return super.put(key, value);
		}
		if(this.keyFilter != null && this.valueFilter != null)
		{
			if(this.keyFilter.accept(key) && this.valueFilter.accept(value))
			{
				return super.put(key, value);
			}
		}
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> map)
	{
		Map<K, V> temp = new HashMap<>();
		for(Entry<? extends K, ? extends V> entry : map.entrySet())
		{
			K key = entry.getKey();
			V value = entry.getValue();
			if(this.mapFilter != null && this.mapFilter.acceptKey(key) && this.mapFilter.acceptValue(value))
			{
				temp.put(key, value);
				continue;
			}
			if(this.keyFilter != null && this.valueFilter != null)
			{
				if(this.keyFilter.accept(key) && this.valueFilter.accept(value))
				{
					temp.put(key, value);
				}
			}
		}
		super.putAll(temp);
	}
}
