package com.infernio.infernocore.proxy;

import java.io.IOException;

import org.apache.logging.log4j.Level;

import com.infernio.infernocore.InfernoCore;
import com.infernio.infernocore.event.ClientEventListener;
import com.infernio.infernocore.key.KeyRegistry;

import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.FMLLog;

public class ClientProxy extends Proxy
{
    @Override
    public void registerListener()
    {
        MinecraftForge.EVENT_BUS.register(new ClientEventListener());
    }

    @Override
    public int addArmor(String armor)
    {
        // TODO What about this?
        // return RenderingRegistry.addNewArmourRendererPrefix(armor);
        return 0;
    }

    @Override
    public void tick()
    {
        try
        {
            FMLClientHandler.instance().getClient().runTick();
        }
        catch(IOException e)
        {
            InfernoCore.log.warn("[Craftmistry] Failed to run client tick due to an exception:", e);
        }
    }

    @Override
    public float getSoundVolume(SoundCategory category)
    {
        return FMLClientHandler.instance().getClient().gameSettings.getSoundLevel(category);
    }

    @Override
    public void registerKeyHandler()
    {
        MinecraftForge.EVENT_BUS.register(new KeyRegistry());
    }

    @Override
    public World[] getCurrentWorlds()
    {
        return new World[]{FMLClientHandler.instance().getWorldClient()};
    }
}
