package com.infernio.infernocore.proxy;

import com.infernio.infernocore.event.EventListener;

import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class Proxy
{
    public void registerListener()
    {
        MinecraftForge.EVENT_BUS.register(new EventListener());
    }

    public void registerKeyHandler() {}

    public void tick()
    {
        FMLCommonHandler.instance().getMinecraftServerInstance().tick();
    }

    public float getSoundVolume(SoundCategory category)
    {
        return 0.5F;
    }

    public int addArmor(String armor)
    {
        return 0;
    }

    public World[] getCurrentWorlds()
    {
        return FMLCommonHandler.instance().getMinecraftServerInstance().worlds;
    }
}
