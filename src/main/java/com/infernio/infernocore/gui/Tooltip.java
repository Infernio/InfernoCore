package com.infernio.infernocore.gui;

import java.util.ArrayList;

import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class Tooltip
{
    public ArrayList<String> tooltip;

    public Tooltip()
    {
        this.tooltip = new ArrayList<>();
    }

    public static Tooltip loadLocalizedTooltip(String tooltipName)
    {
        Tooltip ret = new Tooltip();
        String count = I18n.translateToLocal(tooltipName + ".lineCount");
        if(isInteger(count))
        {
            int lines = Integer.parseInt(count);
            for(int line = 0; line < lines; ++line)
            {
                ret.tooltip.add(resolveColors(I18n.translateToLocal(tooltipName + ".line" + line)));
            }
        }
        else
        {
            ret.tooltip.add(TextFormatting.RED + "INVALID TOOLTIP FORMAT");
            ret.tooltip.add(TextFormatting.RED + "'" + tooltipName + ".lineCount' must exist and be a number!");
        }
        return ret;
    }

    public static String resolveColors(String raw)
    {
        boolean readingColor = false;
        StringBuilder ret = new StringBuilder();
        StringBuilder color = new StringBuilder();
        for(char c : raw.toCharArray())
        {
            if(c == '$')
            {
                if(readingColor)
                {
                    readingColor = false;

                    // TODO Will probably have to change this to toUpperCase(locale)
                    TextFormatting format = TextFormatting.valueOf(color.toString().toUpperCase());
                    if(format != null)
                    {
                        ret.append(format.toString());
                    }
                    color = new StringBuilder();
                }
                else
                {
                    readingColor = true;
                }
            }
            else if(readingColor)
            {
                color.append(c);
            }
            else
            {
                ret.append(c);
            }
        }
        return ret.toString();
    }

    private static boolean isInteger(String s)
    {
        for(char c : s.toCharArray())
        {
            if(!Character.isDigit(c))
            {
                return false;
            }
        }
        return true;
    }

    public void addLine(String line)
    {
        this.tooltip.add(I18n.translateToLocal(line));
    }

    public void addUnlocalizedLine(String line)
    {
        this.tooltip.add(line);
    }

}
