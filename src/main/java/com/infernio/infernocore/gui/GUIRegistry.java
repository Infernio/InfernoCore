package com.infernio.infernocore.gui;

import java.util.HashMap;

import com.infernio.infernocore.InfernoCore;

public class GUIRegistry
{
    private static HashMap<Short, Object> modMap = new HashMap<>();
    private static HashMap<Object, Short> idMap = new HashMap<>();
    private static short freeID = -1;

    public static short getFreeID()
    {
        if(freeID == Short.MAX_VALUE)
        {
            InfernoCore.log.fatal("[GUIRegistry] Ran out of free IDs! Have you actually got 32767 mods installed that all use InfernoCore?!");
            return -1;
        }
        return ++freeID;
    }

    public static void registerMod(short id, Object mod)
    {
        if(id < 0)
        {
            throw new IllegalArgumentException("[InfernoCore] [GUIRegistry] The specified id " + id + " is invalid! Mod GUI IDs must be smaller than 32767 and positive.");
        }
        Short key = Short.valueOf(id);
        if(modMap.containsKey(key))
        {
            throw new IllegalArgumentException("[InfernoCore] [GUIRegistry] ID " + id + " is already occupied with: " + modMap.get(key));
        }
        modMap.put(key, mod);
        idMap.put(mod, key);
    }

    public static Object getMod(short id)
    {
        return modMap.get(Short.valueOf(id));
    }

    public static short getID(Object mod)
    {
        Short ret = idMap.get(mod);
        if(ret != null)
        {
            return ret.shortValue();
        }
        return -1;
    }
}
