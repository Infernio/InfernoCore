package com.infernio.infernocore.client.gui;

import java.util.Iterator;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.infernocore.tile.IFluidTile;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIUtils
{
    private static float zLevel = 0.0F;
    private static int color1 = 1347420415;
    private static int color2 = (color1 & 16711422) >> 1 | color1 & -16777216;
    private static int backgroundColor = -267386864;
    private static int stringColor = -1;
    public static final FontRenderer fontRenderer = FMLClientHandler.instance().getClient().fontRenderer;

    public static boolean intersects(int targetX, int targetY, int mouseX, int mouseY, int width, int height, int guiLeft, int guiTop)
    {
        mouseX -= guiLeft;
        mouseY -= guiTop;
        return mouseX >= targetX - 1 && mouseX < targetX + width + 1 && mouseY >= targetY - 1 && mouseY < targetY + height + 1;
    }

    public static boolean isMouseOverSlot(Slot slot, int x, int y, int guiLeft, int guiTop)
    {
        return intersects(slot.xPos, slot.yPos, x, y, 16, 16, guiLeft, guiTop);
    }

    public static Slot getSlotAtPosition(Container container, int x, int y, int guiLeft, int guiTop)
    {
        for(int i = 0; i < container.inventorySlots.size(); ++i)
        {
            Slot slot = container.inventorySlots.get(i);
            if(isMouseOverSlot(slot, x, y, guiLeft, guiTop))
            {
                return slot;
            }
        }
        return null;
    }

    public static void drawHoveringText(List<String> list, int x, int y, FontRenderer fontRenderer, int guiHeight, int guiWidth)
    {
        if(!list.isEmpty())
        {
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            int biggestWidth = 0;
            Iterator<String> iterator = list.iterator();
            while(iterator.hasNext())
            {
                String s = iterator.next();
                int width = fontRenderer.getStringWidth(s);
                if(width > biggestWidth)
                {
                    biggestWidth = width;
                }
            }
            int x1 = x + 12;
            int y1 = y - 12;
            int k1 = 8;
            if(list.size() > 1)
            {
                k1 += 2 + (list.size() - 1) * 10;
            }
            if(x1 + biggestWidth > guiWidth)
            {
                x1 -= 28 + biggestWidth;
            }
            if(y1 + k1 + 6 > guiHeight)
            {
                y1 = guiHeight - k1 - 6;
            }
            zLevel = 300.0F;
            drawGradientRect(x1 - 3, y1 - 4, x1 + biggestWidth + 3, y1 - 3, backgroundColor, backgroundColor);
            drawGradientRect(x1 - 3, y1 + k1 + 3, x1 + biggestWidth + 3, y1 + k1 + 4, backgroundColor, backgroundColor);
            drawGradientRect(x1 - 3, y1 - 3, x1 + biggestWidth + 3, y1 + k1 + 3, backgroundColor, backgroundColor);
            drawGradientRect(x1 - 4, y1 - 3, x1 - 3, y1 + k1 + 3, backgroundColor, backgroundColor);
            drawGradientRect(x1 + biggestWidth + 3, y1 - 3, x1 + biggestWidth + 4, y1 + k1 + 3, backgroundColor, backgroundColor);
            drawGradientRect(x1 - 3, y1 - 3 + 1, x1 - 3 + 1, y1 + k1 + 3 - 1, color1, color2);
            drawGradientRect(x1 + biggestWidth + 2, y1 - 3 + 1, x1 + biggestWidth + 3, y1 + k1 + 3 - 1, color1, color2);
            drawGradientRect(x1 - 3, y1 - 3, x1 + biggestWidth + 3, y1 - 3 + 1, color1, color1);
            drawGradientRect(x1 - 3, y1 + k1 + 2, x1 + biggestWidth + 3, y1 + k1 + 3, color2, color2);
            for(int i = 0; i < list.size(); ++i)
            {
                String s1 = list.get(i);
                fontRenderer.drawStringWithShadow(s1, x1, y1, stringColor);
                if(i == 0)
                {
                    y1 += 2;
                }
                y1 += 10;
            }
            zLevel = 0.0F;
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            RenderHelper.enableStandardItemLighting();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        }
    }

    public static void drawGradientRect(int x1, int y1, int x2, int y2, int color1, int color2)
    {
        float color1Alpha = (color1 >> 24 & 255) / 255.0F;
        float color1Red = (color1 >> 16 & 255) / 255.0F;
        float color1Green = (color1 >> 8 & 255) / 255.0F;
        float color1Blue = (color1 & 255) / 255.0F;
        float color2Alpha = (color2 >> 24 & 255) / 255.0F;
        float color2Red = (color2 >> 16 & 255) / 255.0F;
        float color2Green = (color2 >> 8 & 255) / 255.0F;
        float color2Blue = (color2 & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
        buffer.pos(x2, y1, zLevel).color(color1Red, color1Green, color1Blue, color1Alpha).endVertex();
        buffer.pos(x1, y1, zLevel).color(color1Red, color1Green, color1Blue, color1Alpha).endVertex();
        buffer.pos(x1, y2, zLevel).color(color2Red, color2Green, color2Blue, color2Alpha).endVertex();
        buffer.pos(x2, y2, zLevel).color(color2Red, color2Green, color2Blue, color2Alpha).endVertex();
        tessellator.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    public void drawTexturedModalRect(int x, int y, int u, int v, int width, int height)
    {
        float scale = 0.00390625F;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(x, y + height, zLevel).tex(u * scale, (v + height) * scale).endVertex();
        buffer.pos(x + width, y + height, zLevel).tex((u + width) * scale, (v + height) * scale).endVertex();
        buffer.pos(x + width, y, zLevel).tex((u + width) * scale, v * scale).endVertex();
        buffer.pos(x, y, zLevel).tex(u * scale, v * scale).endVertex();
        tessellator.draw();
    }

    public static float scaleTank(IFluidTile tile)
    {
        int capacity = tile.getCapacity();
        return Math.min(tile.getFluidAmount(), capacity) / (float)capacity;
    }

    public static void setZLevel(float to)
    {
        zLevel = to;
    }

    public static void setColorSelection(int newColor1, int newColor2)
    {
        color1 = newColor1;
        color2 = newColor2;
    }

    public static void setBackgroundColor(int newColor)
    {
        backgroundColor = newColor;
    }

    public static void setStringColor(int newColor)
    {
        stringColor = newColor;
    }

    public static void resetColorSelection()
    {
        color1 = 1347420415;
        color2 = (color1 & 16711422) >> 1 | color1 & -16777216;
        backgroundColor = -267386864;
        stringColor = -1;
    }
}
