package com.infernio.infernocore.client.icon;

import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public interface ITextureSubscriber
{
    void loadTextures(TextureMap map);

    void postLoadTextures(TextureMap map);

    boolean shouldPostLoad();
}
