package com.infernio.infernocore.client.icon;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class TextureRegistry
{
    private List<ITextureSubscriber> subscribers = new ArrayList<>();

    private static TextureRegistry instance = new TextureRegistry();

    private TextureRegistry() {}

    public static TextureRegistry getInstance()
    {
        return instance;
    }

    public void registerSubscriber(ITextureSubscriber subscriber)
    {
        if(!this.subscribers.contains(subscriber))
        {
            this.subscribers.add(subscriber);
        }
    }

    public void loadTextures(TextureMap map)
    {
        if(map != null)
        {
            for(ITextureSubscriber subscriber : this.subscribers)
            {
                if(subscriber != null)
                {
                    subscriber.loadTextures(map);
                }
            }
        }
    }

    public void postLoadTextures(TextureMap map)
    {
        if(map != null)
        {
            for(ITextureSubscriber subscriber : this.subscribers)
            {
                if(subscriber != null && subscriber.shouldPostLoad())
                {
                    subscriber.postLoadTextures(map);
                }
            }
        }
    }
}
