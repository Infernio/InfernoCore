package com.infernio.infernocore.client.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderUtils
{
    private static RenderEntityItem itemRenderer = new RenderEntityItem(FMLClientHandler.instance().getClient().getRenderManager(), FMLClientHandler.instance().getClient().getRenderItem());

    public static void renderItemInWorld(ItemStack item, World world, double x, double y, double z, boolean spinning, int speed)
    {
        if(item != null && item.getItem() != null)
        {
            float rotation = 0.0F;
            if(spinning)
            {
                rotation = (float)(720.0F * (double)(System.currentTimeMillis() & 0x3FFFL) / 0x3FFFL * speed);
            }
            EntityItem entity = new EntityItem(world);
            entity.hoverStart = 0.0F;
            entity.setItem(item);
            if(entity.getItem() != null && entity.getItem().getItem() != null)
            {
                if(Block.getBlockFromItem(item.getItem()) != Blocks.AIR)
                {
                    GL11.glTranslatef((float)x + 0.5F, (float)y + 0.25F, (float)z + 0.5F);
                }
                else
                {
                    GL11.glTranslatef((float)x + 0.5F, (float)y + 0.125F, (float)z + 0.5F);
                }
                if(Block.getBlockFromItem(item.getItem()) != Blocks.AIR)
                {
                    GL11.glScalef(0.9F, 0.9F, 0.9F);
                }
                else
                {
                    GL11.glScalef(0.6F, 0.6F, 0.6F);
                }
                if(spinning)
                {
                    GL11.glRotatef(rotation, 0.0F, 1.0F, 0.0F);
                }
                itemRenderer.doRender(entity, 0, 0, 0, 0, 0);
            }
        }
    }

    public static void setupItemRenderer(RenderEntityItem renderer)
    {
        itemRenderer = renderer;
    }
}
