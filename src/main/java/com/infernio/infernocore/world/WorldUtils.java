package com.infernio.infernocore.world;

import com.infernio.infernocore.InfernoCore;
import com.infernio.infernocore.multiblock.TileMultiblock;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class WorldUtils
{
    public static String getBlockKey(String blockName)
    {
        return blockName.startsWith("tile.") ? blockName + ".name" : "tile." + blockName + ".name";
    }

    public static Material getMaterial(IBlockAccess world, BlockPos pos)
    {
        return world.getBlockState(pos).getMaterial();
    }

    public static boolean isAirBlock(IBlockAccess world, BlockPos pos)
    {
        IBlockState state = world.getBlockState(pos);
        if(state != null && state.getBlock() != null)
        {
            return state.getBlock().isAir(state, world, pos);
        }
        return true;
    }

    public static boolean canOreGenReplace(World world, BlockPos pos, Block target)
    {
        IBlockState state = world.getBlockState(pos);
        if(state != null && state.getBlock() != null)
        {
            return state.getBlock().isReplaceableOreGen(state, world, pos, BlockMatcher.forBlock(target));
        }
        return false;
    }

    public static boolean tileFits(IBlockAccess world, BlockPos pos, TileMultiblock part)
    {
        TileEntity tile = world.getTileEntity(pos);
        if(tile instanceof TileMultiblock)
        {
            TileMultiblock other = (TileMultiblock)tile;
            return part.getID().equals(other.getID());
        }
        return false;
    }

    public static void markNeighborsRenderUpdate(World world, int x, int y, int z)
    {
        world.markBlockRangeForRenderUpdate(x - 1, y - 1, z - 1, x + 1, y + 1, z + 1);
    }

    public static BlockPos moveByFacing(BlockPos pos, EnumFacing facing)
    {
        switch(facing)
        {
        case DOWN:
            pos.down();
            break;
        case UP:
            pos.up();
            break;
        case EAST:
            pos.east();
            break;
        case NORTH:
            pos.north();
            break;
        case SOUTH:
            pos.south();
            break;
        case WEST:
            pos.west();
            break;
        default:
            InfernoCore.log.warn("[InfernoCore] Unknown facing '" + facing + "'.");
            break;
        }
        return pos;
    }
}
