package com.infernio.infernocore.world;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

// TODO Use WorldGenMinable instead
public class WorldGenCustomOre
{
    private Block minableBlock;
    private int minableBlockMeta = 0;

    private int numberOfBlocks;
    private Block targetBlock;

    public WorldGenCustomOre(Block block, int meta, int amount, Block target)
    {
        this.minableBlock = block;
        this.minableBlockMeta = meta;
        this.numberOfBlocks = amount;
        this.targetBlock = target;
    }

    public boolean generate(World world, Random random, int x, int y, int z)
    {
        float f = random.nextFloat() * (float)Math.PI;
        double x1 = x + 8 + MathHelper.sin(f) * this.numberOfBlocks / 8.0F;
        double x2 = x + 8 - MathHelper.sin(f) * this.numberOfBlocks / 8.0F;
        double z1 = z + 8 + MathHelper.cos(f) * this.numberOfBlocks / 8.0F;
        double z2 = z + 8 - MathHelper.cos(f) * this.numberOfBlocks / 8.0F;
        double y1 = y + random.nextInt(3) - 2;
        double y2 = y + random.nextInt(3) - 2;
        for(int i = 0; i <= this.numberOfBlocks; ++i)
        {
            double x3 = x1 + (x2 - x1) * i / this.numberOfBlocks;
            double y3 = y1 + (y2 - y1) * i / this.numberOfBlocks;
            double z3 = z1 + (z2 - z1) * i / this.numberOfBlocks;
            double generated = random.nextDouble() * this.numberOfBlocks / 16.0D;
            double sin1 = (MathHelper.sin(i * (float)Math.PI / this.numberOfBlocks) + 1.0F) * generated + 1.0D;
            double sin2 = (MathHelper.sin(i * (float)Math.PI / this.numberOfBlocks) + 1.0F) * generated + 1.0D;
            int floorX1 = MathHelper.floor(x3 - sin1 / 2.0D);
            int floorY1 = MathHelper.floor(y3 - sin2 / 2.0D);
            int floorZ1 = MathHelper.floor(z3 - sin1 / 2.0D);
            int floorX2 = MathHelper.floor(x3 + sin1 / 2.0D);
            int floorY2 = MathHelper.floor(y3 + sin2 / 2.0D);
            int floorZ2 = MathHelper.floor(z3 + sin1 / 2.0D);
            for(int finalX = floorX1; finalX <= floorX2; ++finalX)
            {
                double xGen = (finalX + 0.5D - x3) / (sin1 / 2.0D);
                if(xGen * xGen < 1.0D)
                {
                    for(int finalY = floorY1; finalY <= floorY2; ++finalY)
                    {
                        double yGen = (finalY + 0.5D - y3) / (sin2 / 2.0D);
                        if(xGen * xGen + yGen * yGen < 1.0D)
                        {
                            for(int finalZ = floorZ1; finalZ <= floorZ2; ++finalZ)
                            {
                                double zGen = (finalZ + 0.5D - z3) / (sin1 / 2.0D);
                                BlockPos finalPos = new BlockPos(finalX, finalY, finalZ);
                                IBlockState state = world.getBlockState(finalPos);
                                if(xGen * xGen + yGen * yGen + zGen * zGen < 1.0D && (state.getBlock().isReplaceableOreGen(state, world, finalPos, BlockMatcher.forBlock(this.targetBlock))))
                                {
                                    world.setBlockState(new BlockPos(finalX, finalY, finalZ), this.minableBlock.getStateFromMeta(this.minableBlockMeta), 2);
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
